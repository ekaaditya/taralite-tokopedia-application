'use strict'

let Bookshelf = require('../config/bookshelf')

var Forms = Bookshelf.Model.extend({
  tableName: 'formToped'
}, {
 transaction: Bookshelf.transaction.bind(Bookshelf)
})

module.exports = Bookshelf.model('forms', Forms)
