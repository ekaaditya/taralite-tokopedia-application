'use strict'

let Bookshelf = require('../config/bookshelf')

var Template = Bookshelf.Model.extend({
  tableName: 'template'
})

module.exports = Bookshelf.model('template', Template)
