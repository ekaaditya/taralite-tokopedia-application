'use strict'

// var path = require('path')
var fs = require('fs')
// var Printer = require('pdfmake')
var Pdf = require('pdfkit')
var PdfTable = require('voilab-pdf-table')
var blobStream = require('blob-stream')

// function fontPath (file) {
//   return path.resolve('pdfmake', 'test-env', 'tests', 'fonts', file)
// }
// var stream = doc.pipe(blobStream())
function suratHutang (data, cb) {
  var doc = new Pdf()
  var stream = doc.pipe(blobStream())
  var dir = 'generate_pdf/'
  var bold = 'Times-Bold'
  // var italic = 'Times-Italic'
  var normal = 'Times-Roman'
  var suratHutangFilename = dir + 'SERTIFIKAT_SURAT_HUTANG_' + data.name + '.pdf'
  // var widthTab = doc.widthOfString('Tanggal Penerbitan  ')
  // var verdanaFont = 'public/assets/font/Verdana.ttf'
  // var verdanaBoldFont = 'public/assets/font/verdanab.ttf'
  var p1 = 'Sertifikat Surat Hutang ("Sertifikat") ini diterbitkan oleh ("Penerbit") dengan syarat-syarat dan ketentuan yang diatur di dalam, dan merupakan bagian yang tidak terpisahkan dari, Perjanjian Penerbitan Surat Hutang tanggal ("Perjanjian") antara Penerbit dengan PEMESAN yang terdaftar dalam Lampiran 1 dari Perjanjian ("Pemesan") dan PT Indonusa Bara Sejahtera selaku Agen Fasilitas yang dalam hal ini telah diberikan kuasa dari Pemesan untuk memegang Surat Hutang untuk dan atas kepentingan dari Pemesan ("Pemegang Surat Hutang").'
  var p2 = 'Penerbit dengan ini menegaskan bahwa Pemegang Surat Hutang pada tanggal Sertifikat ini merupakan pemegang Surat Hutang yang sah dengan jumlah pokok Rp. Atas nilai yang diterima tersebut, Penerbit berjanji untuk melakukan pelunasan Kewajiban yang Terhutang sesuai dengan ketentuan yang diatur di dalam Perjanjian.'
  var p3 = 'Sertifikat ini hanya merupakan bukti kepemilikan. Penerbit dengan ini menyatakan dan menegaskan bahwa Sertifikat ini tidak ditawarkan kepada masyarakat dan Penerbit tidak pernah mengajukan permohonan pendaftaran kepada Otoritas Jasa Keuangan sesuai dengan Undang-Undang No. 8 tahun 1995 tentang Pasar Modal dan peraturan pelaksananya. Oleh karena itu, Sertifikat tidak dapat ditawarkan atau dijual dengan cara yang merupakan penawaran umum berdasarkan hukum yang berlaku di Indonesia.'
  var p4 = 'Sertifikat ini diatur berdasarkan, dan harus ditafsirkan sesuai dengan peraturan perundang-undangan yang berlaku di Republik Indonesia.'
  doc.fontSize(14)
    .font(bold)
    .text('SERTIFIKAT SURAT HUTANG', {
      align: 'center'})
    .moveDown(3)
  doc.font(bold, 12)
     .text('Jumlah Pokok', {continued: true})
     .text(':', {continued: true})
     .text(data.amount)
     .moveDown(0.5)
  doc.font(bold, 12)
     .text('Tenor', {continued: true})
     .text(':', {continued: true})
     .text(data.term)
     .moveDown(0.5)
  doc.font(bold, 12)
     .text('Angsuran', {continued: true})
     .text(':', {continued: true})
     .text(data.payment)
     .moveDown(0.5)
  doc.font(bold, 12)
     .text('Bunga (flat)', {continued: true})
     .text(':', {continued: true})
     .text(data.interest)
     .moveDown(0.5)
  doc.font(normal, 12)
     .text('No', {continued: true})
     .text(':', {continued: true})
     .text(data.numContract)
     .moveDown(0.5)
  doc.font(normal, 12)
     .text('Tanggal Penerbitan', {continued: true})
     .text(':', {continued: true})
     .text(data.releaseDate)
     .moveDown()

  doc.font(normal).text(p1, {
    align: 'justify'
  }).moveDown()
  doc.text(p2, {
    align: 'justify'
  }).moveDown()
  doc.text(p3, {
    align: 'justify'
  }).moveDown()
  doc.text(p4, {
    align: 'justify'
  }).moveDown()
  doc.text('12 Desember 2016').moveDown()
  doc.text('Penerbit:').moveDown(3)
  doc.text(data.name)
  doc.pipe(fs.createWriteStream(suratHutangFilename))
  doc.end()
  stream.on('finish', function () {
    var result = {message: 'pdf successfully generate', success: true}
    cb(result)
  })
}

function pnIssuanceTokopedia (data, cb) {
  var doc = new Pdf()
  var stream = doc.pipe(blobStream())
  var dir = 'generate_pdf/'
  var bold = 'Times-Bold'
  var italic = 'Times-Italic'
  var normal = 'Times-Roman'
  var pnIssuaceFilename = dir + 'PN_ISSUANCE_AGREEMENT(TOKOPEDIA)_' + data.name + '.pdf'
  doc.fontSize(12)
     .font(normal)
     .text('PERJANJIAN PENERBITAN SURAT HUTANG ("Perjanjian") ini dibuat pada tanggal 2016, antara:', 100, 100).moveDown()

  doc.text('1', {continued: true}).text('Jonathan, Warga Negara Indonesia, pemegang Kartu Tanda Penduduk No. , yang berdomisili di ("Penerbit")', 120, null, {align: 'justify'}).moveDown(0.5)
  doc.text('2', {continued: true}).text('PT Indonusa Bara Sejahtera, suatu perseroan terbatas yang didirikan dan tunduk berdasarkan Hukum yang Berlaku di Indonesia, yang berdomisili di DBS Tower ("Agen Fasilitas") dan', 120, null, {align: 'justify'}).moveDown(0.5)
  doc.text('3', {continued: true}).text('PARA PEMESAN yang tercantum dalam Lampiran 1 dari Perjanjian ini, yang dalam hal ini telah memberikan kuasa kepada Agen Fasilitas untuk melakukan pemesanan atas Surat Hutang ("Pemesan"),', 120, null, {align: 'justify'}).moveDown(0.5)
  doc.text('Penerbit, Agen Fasilitas dan Pemesan dapat disebut secara bersama-sama sebagai "Para Pihak" dan masing-masing sebagai "Pihak".').moveDown()

  doc.text('LATAR BELAKANG').moveDown(0.5)
  doc.text('(A)', {continued: true}).text('Penerbit bermaksud untuk menerbitkan dan menawarkan suatu surat hutang kepada Pemesan dalam jumlah sebesar sebagaimana tercantum di Lampiran 2 Perjanjian ini, yang mana bukti surat hutang tersebut adalah sertifikat surat hutang yang dibuat dengan bentuk sebagaimana tersirat di Lampiran 3 Perjanjian ini ("Surat Hutang") kepada Pemesan dengan tunduk ketentuan Perjanjian ini.', 130, null, {align: 'justify'}).moveDown(0.5)

  doc.text('(B)', {continued: true}).text('Sehubungan dengan penerbitan dan pemesanan Surat Hutang yang dilakukan dari Penerbit kepada Pemesan, Pemesan dengan ini setuju untuk memesan dan membayar Surat Hutang dengan menunjuk Agen Fasilitas untuk dan atas nama Pemesan melakukan segala tindakan yang diperlukan sehubungan dengan memfasilitasi pemesanan, penerbitan, dan hal-hal administrasi lainnya sehubungan dengan Surat Hutang sesuai dengan ketentuan yang diatur di dalam Perjanjian ini.', 130, null, {align: 'justify'}).moveDown(0.5)

  doc.text('(C)', {continued: true}).text('Pembagian hak dan kewajiban diantara Pemesan atas Surat Hutang yang diterbitkan adalah secara proporsional sesuai dengan besaran partisipasi tiap-tiap Pemesan sebagaimana tercantum di Lampiran 1 Perjanjian ini.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.font(bold).text('OLEH KARENA ITU, TELAH DISEPAKATI SEBAGAI BERIKUT:').moveDown(0.5)
  doc.text('1.', {continued: true}).text('DEFINISI DAN INTERPRETASI', 120, null, {align: 'justify'}).moveDown(0.5)
  doc.font(normal)
  doc.text('1.1.', {continued: true}).text('Dalam Perjanjian ini, selain dinyatakan lain secara tegas:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.font(bold).text('“Angsuran” ', 130, null, {continued: true, align: 'justify'}).font(normal).text('adalah Kewajiban yang Terhutang, yang harus dibayar secara berkala oleh Penerbit kepada Pemesan melalui Agen Fasilitas sesuai dengan Jadwal Pembayaran Angsuran melalui pembayaran tunai terhadap Rekening Agen Fasilitas.').moveDown(0.5)
  doc.font(bold).text('“Entitas” ', 130, null, {continued: true, align: 'justify'}).font(normal).text('berarti setiap individu, perusahaan/korporasi, perseroan terbatas, kemitraan umum (general partnership), kemitraan terbatas (limited partnership), usaha perseorangan, bentuk organisasi bisnis lainnya, wali amanat (trust), serikat, asosiasi, atau Otoritas Pemerintahan.').moveDown(0.5)
  doc.addPage({margin: {top: 40,
      bottom: 50,
      left: 72,
      right: 72}})
  doc.font(bold).text('“Harga Penerbitan” ', 130, null, {continued: true, align: 'justify'}).font(normal).text('berarti nominal jumlah hutang yang tercantum di Surat Hutang yang diterbitkan sesuai dengan bentuk sebagaimana tersirat di Lampiran 3 Perjanjian ini.').moveDown(0.5)
  doc.font(bold).text('“Hari Kerja” ', 130, null, {continued: true, align: 'justify'}).font(normal).text('berarti setiap hari.').moveDown(0.5)
  doc.font(bold).text('“Hukum yang Berlaku” ', 130, null, {continued: true, align: 'justify'}).font(normal).text('berarti setiap statuta, undang-undang, regulasi, peraturan (termasuk peraturan pencatatan di bursa efek), Perintah, dan bentuk aturan lainnya yang diakui sebagai hukum yang secara sah berlaku dalam suatu yurisdiksi.').moveDown(0.5)
  doc.font(bold).text('“Jadwal Pembayaran Angsuran” ', 130, null, {continued: true, align: 'justify'}).font(normal).text('jadwal pembayaran Angsuran dari Penerbit yang jatuh pada Tanggal Pembayaran Angsuran, yang rinciannya disebutkan pada Lampiran 2 Perjanjian ini.').moveDown(0.5)
  doc.font(bold).text('“Kerugian” ', 130, null, {continued: true, align: 'justify'}).font(normal).text('berarti setiap bentuk kerusakan dan kerugian yang diderita dan/atau ditanggung secara aktual oleh suatu Entitas.').moveDown(0.5)
  doc.font(bold).text('“Kewajiban” ', 130, null, {continued: true, align: 'justify'}).font(normal).text('berarti segala bentuk kewajiban, klaim, kerugian, kerusakan, komitmen, dan tanggung jawab, baik yang diketahui maupun yang tidak diketahui, yang kontingen ', {continued: true}).font(italic).text('(contigent) ', {continued: true}).font(normal).text('maupun  yang tidak, dari suatu Entitas, berdasarkan atau sehubungan dengan antara lain: perjanjian-perjanjian, Pajak, hutang, dan kewajiban pembayaran yang masih terhutang yang berhubungan dengan kegiatan usaha Entitas tersebut.').moveDown(0.5)
  doc.font(bold).text('“Kewajiban yang Terhutang” ', 130, null, {continued: true, align: 'justify'}).font(normal).text('berarti seluruh Kewajiban yang ada pada saat ini dan di kemudian hari termasuk tetapi tidak terbatas pada suatu jumlah (baik jumlah pokok Surat Hutang, bunga, biaya dan hal-hal lainnya) yang terhutang oleh Penerbit kepada Pemesan.').moveDown(0.5)
  doc.font(bold).text('“Otoritas Pemerintahan” ', 130, null, {continued: true, align: 'justify'}).font(normal).text('berarti pemerintah dari negara apapun atau subdivisi politik lainnya (baik pada tingkat pusat, negara bagian, maupun daerah), yang meliputi: kementerian, agensi, otoritas, badan pengatur, pengadilan, bank sentral atau entitas lainnya yang menjalankan kewenangan di bidang eksekutif, legislatif, yudisial, Perpajakan, moneter, atau administratif, atau menjalankan fungsi yang terkait dengan kegiatan pemerintah sebagaimana diatur dalam Hukum yang Berlaku dari negara yang relevan.').moveDown(0.5)
  doc.font(bold).text('“Pajak” ', 130, null, {continued: true, align: 'justify'}).font(normal).text('berarti segala bentuk perpajakan, pajak kekayaan, pengurangan, pajak potongan ', {continued: true}).font(italic).text('(wittholding)', {continued: true}).font(normal).text(', bea, pembebanan, pungutan, biaya, ongkos, kontribusi jaminan sosial, pendapatan modal, PPN, dan suku bunga yang dikenakan, dipungut, ditagih, dipotong atau dinilai oleh Otoritas Pemerintahan di Indonesia atau dimanapun dan bunga, pajak tambahan, penalti, biaya tambahan atau denda yang terkait dan "Perpajakan" akan diartikan dengan merujuk ke istilah Pajak.').moveDown(0.5)
  doc.font(bold).text('“Penutupan” ', 130, null, {continued: true, align: 'justify'}).font(normal).text('berarti penutupan pelaksanaan Pemesanan Surat Hutang sebagaimana diatur dalam Perjanjian ini.').moveDown(0.5)
  doc.font(bold).text('“Perintah” ', 130, null, {continued: true, align: 'justify'}).font(normal).text('berarti setiap bentuk surat perintah, putusan (termasuk putusan yang melarang atau mewajibkan pihak untuk melakukan suatu tindakan (injunction)), keputusan, atau perintah lainnya yang sejenis dari Otoritas Pemerintahan (baik yang bersifat permulaan maupun final).').moveDown(0.5)
  doc.font(bold).text('“Perkara” ', 130, null, {continued: true, align: 'justify'}).font(normal).text('berarti setiap (i) sengketa, tindakan, gugatan, atau perkara yang dilakukan melalui atau di luar pengadilan, majelis arbitrase, atau alternatif penyelesaian sengketa lainnya, atau (ii) investigasi atau audit yang dilakukan oleh Otoritas Pemerintahan.').moveDown(0.5)
  doc.font(bold).text('“Rp” ', 130, null, {continued: true, align: 'justify'}).font(normal).text('berarti Rupiah Indonesia, mata uang Republik Indonesia yang sah.').moveDown(0.5)
  doc.font(bold).text('“Sertifikat Surat Hutang” ', 130, null, {continued: true, align: 'justify'}).font(normal).text('berarti sertifikat yang diterbitkan oleh Penerbit kepada Pemesan atas nama Agen Fasilitas (yang dalam kapasitasnya akan memegang Sertifikat Surat Hutang tersebut untuk dan atas kepentingan dari Agen Fasilitas) yang merupakan bagian yang tidak terpisahkan dari Perjanjian ini.').moveDown(0.5)
  doc.font(bold).text('“Surat Pemberitahuan Wanprestasi” ', 130, null, {continued: true, align: 'justify'}).font(normal).text('memiliki arti sebagaimana didefinisikan dalam Pasal 9.2.').moveDown(0.5)
  doc.font(bold).text('“Tanggal Pembayaran Angsuran” ', 130, null, {continued: true, align: 'justify'}).font(normal).text('berarti tanggal dimana suatu Angsuran menjadi jatuh tempo.').moveDown(0.5)
  doc.font(bold).text('“Tanggal Penutupan” ', 130, null, {continued: true, align: 'justify'}).font(normal).text('berarti tanggal yang jatuh pada selambat-lambatnya 2 Hari Kerja setelah terpenuhinya seluruh persyaratan Pemesanan Surat Hutang sebagaimana dimaksud dalam Pasal 4.1 (kecuali ada yang dikesampingkan oleh Pemesan), atau pada tanggal lain sebagaimana disetujui secara tertulis oleh Para Pihak.').moveDown(0.5)
  doc.text('1.2.', 100, null, {continued: true}).text('Setiap rujukan-rujukan, tersurat maupun tersirat, atas Hukum yang Berlaku meliputi seluruh legislasi yang didelegasikan dari Hukum yang Berlaku tersebut dan segala perubahan, konsolidasi, penggantian, atau pengundangan kembali dari Hukum yang Berlaku tersebut.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('1.3.', 100, null, {continued: true}).text('Referensi dalam perjanjian ini untuk Latar Belakang, Pasal-Pasal dan Lampiran-Lampiran adalah referensi terhadap Latar Belakang, Pasal-Pasal dan Lampiran-Lampiran dari Perjanjian ini (kecuali dinyatakan sebaliknya secara tegas). Lampiran-Lampiran Perjanjian ini merupakan bagian yang tidak terpisahkan dari Perjanjian ini.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('1.4.', 100, null, {continued: true}).text('Judul-Judul dimasukkan hanya untuk kenyamanan pembacaan dan tidak akan mempengaruhi penafsiran dari Perjanjian ini.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('1.5.', 100, null, {continued: true}).text('Kapanpun Perjanjian ini merujuk pada suatu tanggal, tanggal tersebut akan merujuk pada hari kalendar Gregorian kecuali dinyatakan sebagai Hari Kerja. Rujukan kepada bulan dan tahun juga merujuk kepada kalender Gregorian.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('1.6.', 100, null, {continued: true}).text('Istilah "Para Pihak" dan "Pihak", sepanjang konteksnya memungkinkan, meliputi setiap penerusnya yang sah atau pihak penerima pengalihan yang diizinkan berdasarkan Perjanjian ini dan Entitas lainnya yang berhak berdasarkan Perjanjian ini atau Hukum yang Berlaku.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('2.', {continued: true}).text('PENERBITAN DAN PEMESANAN SURAT HUTANG', 120, null, {align: 'justify'}).moveDown(0.5)
  doc.text('2.1.', {continued: true}).text('Dengan tunduk pada ketentuan dan persyaratan dalam Perjanjian ini, Penerbit dengan ini setuju untuk menerbitkan Surat Hutang, dan Pemesan telah sepakat untuk memesan Surat Hutang tersebut.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('2.2.', {continued: true}).text('Para Pihak sepakat atas hal-hal sebagai berikut:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', {continued: true}).text('Untuk tujuan pemesanan Surat Hutang yang akan diterbitkan Penerbit, Pemesan (melalui Agen Fasilitas) akan melakukan pembayaran kepada Penerbit sesuai dengan Harga Penerbitan.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', {continued: true}).text('Bersamaan dengan diterimanya pelunasan pembayaran atas Harga Penerbitan oleh Penerbit, Penerbit wajib menerbitkan Surat Hutang kepada Pemesan dengan cara memberikan Sertifikat Surat Hutang kepada Pemesan. Untuk keperluan penerbitan Sertifikat Surat Hutang tersebut, Pemesan sepakat bahwa Sertifikat Surat Hutang tersebut dapat diterbitkan oleh Penerbit kepada Agen Fasilitas yang akan memegang Sertifikat Surat Hutang Tersebut untuk dan atas kepentingan dari Pemesan.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(c)', {continued: true}).text('Dana yang diperoleh dari penerbitan Surat Hutang wajib digunakan oleh Penerbit untuk keperluan sebagaimana yang sebelumnya telah disetujui oleh Pemesan.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(d)', {continued: true}).text('Baik Pemesan maupun Agen Fasilitas tidak terikat untuk memantau atau memeriksa penggunaan jumlah dana yang dipinjam berdasarkan Perjanjian ini.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('3.', {continued: true}).text('SURAT HUTANG', 120, null, {align: 'justify'}).moveDown(0.5)
  doc.text('3.1.', {continued: true}).text('Surat Hutang yang diterbitkan oleh Penerbit memiliki nilai pokok sebesar sebagaimana tercantum di dalam Surat Hutang tersebut.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('3.2.', {continued: true}).text('Surat Hutang ini diterbitkan untuk berlaku selama masa periode tertentu sebagaimana juga tercantum di dalam Surat Hutang dan sebagai bentuk pelunasan dari Kewajiban yang Terhutang, Penerbit berkewajiban untuk melakukan pembayaran atas Angsuran dan tidak dapat meminjam kembali setiap bagian dari Kewajiban yang Terhutang yang telah dibayarkan kembali kepada Pemesan. Sehubungan dengan hal ini, Para Pihak sepakat bahwa Kewajiban pembayaran atas Angsuran tersebut dilakukan wajib dilakukan oleh Penerbit kepada Agen Pemesan yang dalam kapasitasnya sebagai perwakilan dari Pemesan dalam mengatur pelaksanaan pembayaran tersebut. Pembayaran atas Angsuran akan dilakukan dengan mekanisme pembayaran tunai terhadap Rekening Agen Fasilitas.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('3.3.', {continued: true}).text('Surat Hutang ini diterbitkan dengan tingkat suku bunga flat tetap sebagaimana tercantum di Surat Hutang tersebut, yang akan dibayarkan secara berkala, dan dihitung dan wajib dibayarkan sebagai bagian dari Angsuran. Dalam hal bunga Surat Hutang ini dianggap sebagai pendapatan bunga oleh Otoritas Pemerintah yang berwenang (termasuk diantaranya Direktorat Jenderal Pajak), maka kewajiban atas pelaporan kewajiban Pajak tahunan Pemesan adalah menjadi tanggung jawab Pemesan itu sendiri.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('3.4.', {continued: true}).text('Para Pihak sepakat bahwa:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', {continued: true}).text('Dana yang diterima oleh Penerbit dari hasil penerbitan Surat Hutang wajib digunakan oleh Penerbit khusus sebagaimana tercantum di Pasal 2.2 butir c.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', {continued: true}).text('Penerbit menyetujui dan mengakui bahwa kegagalan Penerbit untuk mematuhi Kewajibannya dalam Pasal 3.4 (a) tidak akan mempengaruhi keabsahan dari ketentuan Perjanjian ini dalam bentuk apa pun.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('3.5.', {continued: true}).text('Pada Tanggal Penutupan:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', {continued: true}).text('Penerbit wajib menyampaikan kepada Agen Fasilitas dokumen asli dan salinan atas bukti-bukti yang menunjukkan bahwa seluruh persyaratan sebagaimana diatur dalam Pasal 4.1 telah terpenuhi dimana bukti-bukti tersebut harus dalam bentuk yang dapat diterima oleh Agen Fasilitas.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', {continued: true}).text('Penerbit wajib melakukan hal-hal sebagai berikut:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(i)', {continued: true}).text('menyampaikan informasi kepada Agen Fasilitas mengenai rekening bank Penerbit yang akan menerima pembayaran Harga Penerbitan (“Rekening yang Ditunjuk”); dan', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(ii)', {continued: true}).text('menyampaikan dokumen-dokumen lainnya sebagaimana diminta secara wajar oleh kepada Agen Fasilitas dan/atau Pemesan untuk memastikan pemenuhan Kewajiban Penerbit berdasarkan ketentuan Perjanjian ini.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(c)', {continued: true}).text('Pemesan, melalui Agen Fasilitas, akan menyetorkan komitmen dananya untuk melaksanakan pemesanan Surat Hutang dengan cara melakukan transfer dana senilai Harga Penerbitan ke Rekening yang Ditunjuk setelah ketentuan dalam Pasal 3.5 (a) dan (b) dipenuhi seluruhnya.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(d)', {continued: true}).text(' Bersamaan dengan dilaksanakannya penyetoran dana sebagaimana dimaksud dalam Pasal 3.5 (c), Penerbit wajib menyerahkan kepada Agen Fasilitas, Sertifikat Surat Hutang yang membuktikan kepemilikan Pemesan atas Surat Hutang, yang isinya sesuai dengan bentuk yang ditetapkan dalam Lampiran 3.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('4.', {continued: true}).text('PERSYARATAN-PERSYARATAN', 120, null, {align: 'justify'}).moveDown(0.5)
  doc.text('4.1.', {continued: true}).text('Agar Pemesanan Surat Hutang dapat dilaksanakan, persyaratan-persyaratan di bawah ini wajib dipenuhi terlebih dahulu:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', {continued: true}).text('Pemesan telah mendapatkan seluruh Persetujuan yang dibutuhkan untuk menandatangani Perjanjian ini dan melaksanakan seluruh Kewajibannya berdasarkan Perjanjian ini;', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', {continued: true}).text('seluruh pernyataan dan jaminan berdasarkan Perjanjian ini masih tetap akurat, benar dan tidak menyesatkan baik pada tanggal Perjanjian ini maupun Tanggal Penutupan;', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(c)', {continued: true}).text('seluruh janji-janji dan Kewajiban yang disyaratkan oleh Pemesan dan/atau Agen Fasilitas untuk dilaksanakan atau patuhi oleh Penerbit berdasarkan Perjanjian ini pada atau sebelum Tanggal Penutupan harus telah dilaksanakan dan dipatuhi seluruhnya;', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(d)', {continued: true}).text('tidak terjadi keadaan wanprestasi sebagaimana diatur dalam Pasal 9.1; dan', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(e)', {continued: true}).text('penerbitan dan pemesanan Surat Hutang tidak dilarang berdasarkan Hukum yang Berlaku.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('4.2.', {continued: true}).text('Jika setiap persyaratan yang diatur dalam Pasal 4.1 tidak terpenuhi sampai dengan jangka waktu 1 minggu sejak tanggal Perjanjian ini atau tanggal lain yang disepakati oleh Pemesan dan Penerbit, Perjanjian ini akan segera berakhir dan dalam keadaan tersebut Para Pihak akan dilepaskan dan dibebaskan dari Kewajiban mereka masing-masing berdasarkan Perjanjian ini dan tidak ada Pihak yang akan memiliki klaim apapun terhadap Pihak lain.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('5.', {continued: true}).text('PEMBAYARAN LEBIH AWAL', 120, null, {align: 'justify'}).moveDown(0.5)
  doc.text('5.1.', {continued: true}).text('Jika, setiap saat berdasarkan Hukum Yang Berlaku, Pemesan menjadi tidak sah untuk melakukan salah satu kewajibannya sebagaimana dimaksud dalam Perjanjian ini, termasuk namun tidak terbatas untuk membeli Surat Hutang dari Penerbit, maka', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', {continued: true}).text('Pemesan harus segera memberitahukan kepada Agen Fasilitas setelah mengetahui hal tersebut', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', {continued: true}).text('Agen Fasilitas harus segera memberitahukan hal tersebut kepada Penerbit.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(c)', {continued: true}).text('Penerbit harus segera melunasi Kewajiban yang Terhutang dalam jangka waktu [2] Hari Kerja setelah menerima pemberitahuan dari Agen Fasilitas sebagaimana dimaksud di dalam Pasal 5.1 (ii) di atas, dengan melakukan transfer tunai atas sisa Kewajiban yang Terhutang kepada rekening berikut ini: Nama Pemegang Rekening: PT Indonusa Bara Sejahtera Nomor Rekening: 548-5077798 Nama Bank: BCA KCP Central Park ("Rekening Agen Fasilitas")', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('5.2.', {continued: true}).text('Penerbit setiap waktu diperbolehkan untuk membayar kembali seluruh sisa Kewajiban yang Terhutang sebelum berakhir jangka waktunya, dengan ketentuan Penerbit telah menyampaikan surat pemberitahuan tertulis kepada Agen Fasilitas selambat-lambatnya 7 Hari Kerja sebelumnya tentang kehendak untuk melakukan pembayaran kembali yang dipercepat, yang dengan tegas menyebutkan jumlah seluruh sisa Kewajiban yang Terhutang yang akan dilunasi dan kapan akan dilaksanakan pembayaran tersebut, dengan ketentuan pemberitahuan tertulis tersebut tidak dapat ditarik kembali oleh Penerbit.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('6.', {continued: true}).text('DENDA', 120, null, {align: 'justify'}).moveDown(0.5)
  doc.text('6.1.', {continued: true}).text('Dalam hal Penerbit lalai untuk membayar Kewajibannya berdasarkan Perjanjian, baik Angsuran, sisa Kewajiban yang Terhutang, biaya-biaya dan/atau suatu jumlah lain yang terutang dan wajib dibayar oleh Penerbit kepada Pemesan melalui Agen Fasilitas karena sebab apapun pada tanggal jatuh waktunya sebagaimana ditentukan dalam Perjanjian, maka Penerbit wajib dan setuju untuk membayar denda sebesar [0,1% per hari yang dihitung dari Angsuran] untuk setiap hari keterlambatan.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('6.2.', {continued: true}).text('Pengenaan denda dihitung sejak tanggal jatuh waktu suatu pembayaran sampai dengan tanggal pelunasan Kewajiban tersebut. Denda tersebut harus dibayar secara seketika dan sekaligus lunas bersama dengan pembayaran yang telah lewat waktu dimaksud.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('6.3.', {continued: true}).text('Perhitungan denda tersebut dalam Pasal 6.1 Perjanjian ini dilakukan secara harian atas dasar pembagi tetap 30 hari dalam sebulan.', 130, null, {align: 'justify'}).moveDown(0.5)
  // doc.text('2.2', {continued: true}).text('Para Pihak sepakat atas hal-hal sebagai berikut:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('7.', {continued: true}).text('PERNYATAAN', 120, null, {align: 'justify'}).moveDown(0.5)
  doc.text('7.1.', {continued: true}).text('Penerbit menyatakan kepada Pemesan bahwa terhitung sejak tanggal Perjanjian ini, pernyataan dan informasi sebagaimana dinyatakan di bawah ini adalah benar, tepat dan tidak menyesatkan secara material ("Pernyataan Penerbit"):', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', {continued: true}).text('Penerbit memiliki kekuasaan untuk memiliki asetnya dan memiliki kekuasaan penuh, wewenang dan  hak hukum untuk menerbitkan Surat Hutang dan menandatangani Perjanjian ini.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', {continued: true}).text('Penerbit tidak dalam proses kepailitan, penundaan Kewajiban pembayaran hutang atau proses-proses semacamnya dan tidak ada permohonan untuk kepailitan, penundaan Kewajiban pembayaran hutang atau proses semacamnya yang sedang dilakukan.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(c)', {continued: true}).text('Perjanjian ini dan Surat Hutang dianggap sah, berlaku dan mengikat Kewajiban Penerbit dapat dilaksanakan sesuai dengan ketentuan-ketentuannya.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(d)', {continued: true}).text('Penandatanganan Perjanjian ini dan penerbitan Surat Hutang untuk kepentingan Penerbit.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(e)', {continued: true}).text('seluruh informasi yang diberikan dan dokumen-dokumen yang dikirimkan kepada Pemesan dan Agen Fasilitas oleh atau atas nama Penerbit adalah benar dan akurat dalam semua hal.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(f)', {continued: true}).text('Penerbit umumnya tunduk pada hukum perdata, hukum usaha dan atas Perkara hukum, dan baik Penerbit maupun asetnya atau pendapatannya tidak berhak atas hak kekebalan atau hak istimewa (berdaulat atau sebaliknya) dari setiap penjumpaan utang (set-off), pemutusan, eksekusi, sita jaminan atau proses hukum lainnya.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('7.2.', {continued: true}).text('Penerbit mengakui bahwa Pemesan mengikat Perjanjian ini dengan mengandalkan kebenaran dan keakuratan Pernyataan Penerbit.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('8.', {continued: true}).text('JANJI-JANJI', 120, null, {align: 'justify'}).moveDown(0.5)
  doc.text('8.1.', {continued: true}).text('Selain dari Kewajiban lainnya yang diatur dalam Perjanjian ini, sejak tanggal Perjanjian ini sampai dengan pelunasan penuh Kewajiban yang Terhutang, Penerbit sepakat untuk mematuhi dan melaksanakan seluruh Kewajibannya sebagaimana dinyatakan di bawah ini.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', {continued: true}).text('Penerbit tidak akan mengajukan, dan tidak akan memberikan persetujuan kepada suami atau istrinya untuk mengajukan, permohonan kepailitan atau penundaan Kewajiban pembayaran hutang atau prosedur serupa lainnya atas dirinya sendiri atau atas  suami atau istrinya.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', {continued: true}).text('Penerbit tidak akan menjual atau dengan cara lain mengalihkan, membagikan atau melepaskan semua atau sebagian usahanya atau asset atau pendapatannya, baik dalam satu atau beberapa transaksi yang terkait maupun tidak tidak terkait, yang mungkin mempengaruhi kemampuan Penerbit untuk melakukan pelunasan atas Kewajiban yang Terhutang.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(c)', {continued: true}).text('Penerbit tidak akan mengadakan perjanjian atau kewajiban lainnya yang mungkin menyebabkan Penerbit tidak mampu untuk melakukan pelunasan atas Kewajiban yang Terhutang.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(d)', {continued: true}).text('Penerbit harus memastikan Kewajibannya berdasarkan Perjanjian ini setiap saat paling tidak pari passu dengan utang Penerbit lainnya.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(e)', {continued: true}).text('Penerbit harus tepat waktu membayar semua Kewajiban yang Terhutang yang harus dilunasi sesuai dengan ketentuan yang diatur di dalan Perjanjian ini.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('9.', {continued: true}).text('KEADAAN WANPRESTASI', 120, null, {align: 'justify'}).moveDown(0.5)
  doc.text('9.1.', {continued: true}).text('Salah satu atau lebih dari kejadian-kejadian atau hal-hal-tersebut di bawah ini akan dianggap sebagai wanprestasi dari Penerbit apabila terjadi sebelum dilunasinya Kewajiban yang Terhutang secara keseluruhan. ', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', {continued: true}).text('Penerbit tidak melakukan pembayaran atas seluruh maupun sebagian Kewajiban yang Terhutang yang telah jatuh tempo dan harus dibayar atau terhutang pada saat jatuh tempo sesuai dengan ketentuan-ketentuan dalam Perjanjian ini kecuali kelalaian tersebut disebabkan hal administratif dan teknis, dan pembayaran akhirnya terlaksana dalam waktu 2 Hari Kerja setelah tanggal jatuh tempo yang relevan;', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', {continued: true}).text('Setiap pernyataan atau jaminan yang dibuat oleh Penerbit berdasarkan Pasal 7 ternyata tidak benar, secara material, tidak sesuai dengan kenyataan sebenarnya atau menyesatkan, atau terjadi pelanggaran atas janji-janji berdasarkan Pasal 8 (baik janji untuk melakukan atau janji untuk tidak melakukan) sebagaimana disyaratkan dalam Perjanjian ini dan jika keadaan tersebut dapat diperbaiki, hal itu tidak diperbaiki dalam waktu 5 Hari Kerja setelah Agen Fasilitas menyampaikan pemberitahuan kepada Penerbit; atau', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(c)', {continued: true}).text('Penerbit menyatakan secara tertulis dan secara umum tidak dapat membayar utangnya pada tanggal jatuh tempo atau mengajukan Penundaan Kewajiban Pembayaran Utang (PKPU) ke pengadilan niaga, berhenti atau menghentikan sementara pembayaran-pembayaran kepada kreditur-kreditur atau tidak dapat atau mengakui ketidak sanggupannya untuk membayar hutang-hutangnya pada waktu jatuh tempo atau dinyatakan jatuh pailit.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('9.2.', {continued: true}).text('Jika suatu Wanprestasi telah terjadi maka Agen Fasilitas dapat memberikan surat pemberitahuan Wanprestasi (“Surat Pemberitahuan Wanprestasi”) dan setelahnya (kecuali Penerbit dapat memperbaiki Wanprestasi tersebut sesuai dengan jangka waktu yang diatur di dalam Perjanjian ini) maka Agen Fasilitas berhak memberitahukan secara tertulis kepada Penerbit:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', {continued: true}).text('menyatakan jumlah Kewajiban yang Terhutang yang seketika itu jatuh tempo dan harus dibayar oleh Penerbit tanpa pemberitahuan lebih lanjut; dan/atau', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', {continued: true}).text('menyatakan Perjanjian ini berakhir tanpa mengurangi hak Pemesan dan Agen Fasilitas untuk menuntut hak-haknya berdasarkan Perjanjian ini.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('10.', {continued: true}).text('PENGAKHIRAN', 120, null, {align: 'justify'}).moveDown(0.5)
  doc.text('10.1.', {continued: true}).text('Perjanjian ini dapat diakhiri berdasarkan hal-hal sebagai berikut:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', {continued: true}).text('berdasarkan persetujuan tertulis oleh Para Pihak;', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', {continued: true}).text('terjadinya keadaan Wanprestasi sebagaimana diatur dalam Pasal 9.1; atau', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(c)', {continued: true}).text('dilunasinya seluruh Kewajiban yang Terhutang oleh Penerbit kepada Pemesan, melalui Agen Fasilitas.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('10.2.', {continued: true}).text('Kecuali dinyatakan sebaliknya dalam Pasal 10.3 dan Pasal 10.4, seluruh hak dan Kewajiban Para Pihak akan berakhir dengan segera setelah pengakhiran Perjanjian ini dan tidak ada Pihak yang harus bertanggung jawab terhadap Pihak lainnya atas Kewajiban-Kewajiban apapun sebagai akibat dari pengakhiran dari Perjanjian.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('10.3.', {continued: true}).text('Pengakhiran Perjanjian ini tidak akan berdampak terhadap hak masing-masing Pihak untuk menerima ganti rugi dari Pihak lainnya sehubungan dengan ketentuan dalam Perjanjian ini sepanjang hak tersebut sudah timbul sebelum atau pada saat Perjanjian ini diakhiri.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('10.4.', {continued: true}).text('Ketentuan dalam Pasal 9.2, 10.2, 10.3, 10.4, 11, 12.10 dan 12.11 akan tetap berlaku setelah Perjanjian ini diakhiri.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('10.5.', {continued: true}).text('Para Pihak dengan tidak dapat ditarik kembali setuju untuk mengesampingkan ketentuan Pasal 1266 dari Kitab Undang-Undang Hukum Perdata sepanjang penetapan pengadilan diperlukan untuk pembatalan atau pengakhiran terlebih dahulu dari Perjanjian ini.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('11.', {continued: true}).text('GANTI RUGI', 120, null, {align: 'justify'}).moveDown(0.5)
  doc.text('11.1.', {continued: true}).text('Penerbit wajib memberikan ganti rugi kepada Pemesan dan/atau Agen Fasilitas sehubungan dengan: (i) Kerugian yang secara nyata dialami oleh Pemesan dan/atau Agen Fasilitas sebagai akibat kegagalan Penerbit dalam melaksanakan kewajibannya berdasarkan Perjanjian ini (termasuk segala bentuk wanprestasi sebagaimana diatur dalam Pasal 9.1), dan (ii) seluruh biaya sehubungan dengan dokumentasi (termasuk biaya konsultan hukum yang wajar) dan biaya lainnya yang dikeluarkan oleh Pemesan untuk menuntut hak Pemesan dan/atau Agen Fasilitas atas Kerugian sebagaimana disebutkan di atas.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('11.2.', {continued: true}).text('Semua klaim penggantian Kerugian berdasarkan Pasal 11.1 ini harus dibayarkan oleh Penerbit kepada Pemesan dan/atau Agen Fasilitas dalam jangka waktu 5 Hari Kerja setelah pemberitahuan tertulis terhadap klaim-klaim tersebut berikut dengan bukti-bukti pendukung telah disampaikan oleh Pemesan dan/atau Agen Fasilitas kepada Penerbit.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('12.', {continued: true}).text('LAIN-LAIN', 120, null, {align: 'justify'}).moveDown(0.5)
  doc.text('12.1.', {continued: true}).text('Semua pemberitahuan, permintaan dan bentuk komunikasi lainnya harus dilakukan secara tertulis dalam Bahasa Indonesia dan akan dianggap telah diberikan apabila pemberitahuan tersebut diserahkan secara personal atau dengan transmisi faksimili atau melalui pos (pos prabayar kelas pertama) atau dikirimkan melalui surat elektronik kepada Para Pihak dengan alamat di bawah ini:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('Apabila kepada Pemesan dan/atau Agen Fasilitas:')
  doc.text('PT Indonusa Bara Sejahtera')
  doc.text('Alamat		   : 	Rukan Golden 8 No.8C Jalan Panjang, Kedoya Utara Jakarta Barat, 11520 Indonesia.')
  doc.text('Telp. No.	: 	021 - 2920095')
  doc.text('Fax. No.		:	-')
  doc.text('Email		:	admin@taralite.com')
  doc.text('U.P.		: 	Abraham Viktor')
  doc.text('Apabila kepada Penerbit:')
  doc.text('Alamat		:')
  doc.text('No. Telepon	:')
  doc.text('Setiap Pihak dari waktu ke waktu dapat mengganti alamat, nomor faksimili atau informasi lainnya dengan tujuan untuk melakukan pemberitahuan-pemberitahuan kepada Pihak lainnya dengan memberikan pemberitahuan terkait perubahan informasi tersebut kepada Pihak lainnya.')
  doc.text('12.2', {continued: true}).text('Ketentuan dan kondisi Perjanjian ini dapat dikesampingkan dari waktu ke waktu oleh Para Pihak yang berhak atas pemberian pengesampingan tersebut, tetapi tidak ada pengesampingan yang akan menjadi efektif kecuali pengesampingan tersebut dituangkan dalam instrumen tertulis yang ditandatangani oleh atau atas nama Pihak yang mengesampingkan ketentuan atau kondisi tersebut.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('12.3', {continued: true}).text('Perjanjian ini hanya dapat diubah, ditambah, atau dimodifikasi dengan instrumen tertulis yang ditandatangani secara bersama-sama oleh atau atas nama Para Pihak.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('12.4', {continued: true}).text('Tanpa persetujuan tertulis terlebih dahulu dari Penerbit dan cukup dengan diberikannya suatu pemberitahuan secara tertulis dari Agen Fasilitas kepada Penerbit, Pemesan berhak untuk mengalihkan kepada pihak ketiga sebagian atau seluruh hak dan Kewajibannya berdasarkan Perjanjian ini dengan cara apapun dan Penerbit dengan ini memberikan persetujuannya atas hal tersebut. Penerbit berjanji dan mengikat diri untuk atas permintaan pertama Pemesan, menandatangani, membuat dan menyerahkan dokumen-dokumen yang diminta Pemsan dan melaksanakan perjanjian yang dibuat sehubungan dengan pengalihan dimaksud. Perjanjian tetap mengikat para pengganti hak masing-masing pihak. Selanjutnya Penerbit tidak berhak untuk mengalihkan atau memindahkan dengan cara apapun sebagian atau seluruh hak-hak dan Kewajiban-Kewajibannya berdasarkan Perjanjian tanpa persetujuan tertulis terlebih dahulu dari Pemesan.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('12.5', {continued: true}).text('Apabila ada ketentuan dalam Perjanjian ini yang dinyatakan ilegal, tidak sah, atau tidak dapat dilaksanakan berdasarkan Hukum yang Berlaku saat ini atau masa mendatang, dan apabila hak atau Kewajiban Para Pihak berdasarkan Perjanjian ini tidak akan merugikan secara material:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', {continued: true}).text('ketentuan tersebut akan dipisahkan secara sepenuhnya;', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', {continued: true}).text('Perjanjian ini akan ditafsirkan dan berlaku seakan-akan ketentuan yang ilegal, tidak sah, atau tidak dapat dilaksanakan tersebut tidak pernah menjadi bagian dari Perjanjian ini; dan', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('(c)', {continued: true}).text('ketentuan lainnya dari Perjanjian ini akan tetap berlaku penuh dan tidak akan terpengaruh oleh ketentuan yang ilegal, tidak sah, atau tidak dapat dilaksanakan atau oleh pemisahan dari ketentuan yang bersangkutan.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('12.6', {continued: true}).text('Kecuali diwajibkan berdasarkan peraturan perundang-undangan yang berlaku, Para Pihak setuju untuk tidak mengungkapkan isi Perjanjian kepada pihak ketiga manapun tanpa memperoleh persetujuan terlebih dahulu dari Pihak lainnya.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('12.7', {continued: true}).text('Perjanjian ini dapat ditandatangani dalam beberapa salinan dimana tiap-tiap salinan tersebut dianggap asli dan memiliki keberlakuan yang sama dan salinan-salinan tersebut akan dianggap sebagai satu kesatuan dokumen yang asli dan akan mengikat Para Pihak seakan-akan Perjanjian ini hanya terdiri atas satu dokumen.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('12.8', {continued: true}).text('Perjanjian ini beserta hak-hak dan Kewajiban-Kewajiban yang timbul atau berkenaan dengannya tunduk pada hukum Negara Republik Indonesia.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('12.9', {continued: true}).text('Para pihak dengan ini sepakat bahwa Pengadilan Negeri Jakarta Barat mempunyai yurisdiksi yang non-eksklusif untuk memeriksa dan memutuskan segala gugatan, tuntutan atau tindakan hukum yang timbul berkenaan dengan Perjanjian.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('12.10', {continued: true}).text('Jika setiap Sertifikat Surat Hutang terpotong, rusak, hancur, dicuri atau hilang, sertifikat tersebut dapat diganti oleh Penerbit setelah Agen Fasilitas melakukan pembayaran kepada Penerbit atas biaya yang mungkin dikenakan sehubungan dengan penggantian tersebut, sebagaimana Penerbit dapat minta. Sertifikat Surat Hutang yang rusak atau hancur harus diserahkan kepada Penerbit sebelum penggantinya diterbitkan.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('12.11', {continued: true}).text('Para Pihak sepakat bahwa pembayaran yang dilakukan oleh Penerbit secara tunai (baik dalam konteks melakukan pembayaran terhadap Kewajiban yang Terhutang, Angsuran, ganti Kerugian, dan lainnya) wajib ditujukan kepada Rekening Agen Fasilitas.', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.font(italic).text('[HALAMAN TANDA TANGAN ADA PADA HALAMAN SELANJUTNYA]', 130, null, {align: 'center'}).font(normal).moveDown(0.5)
  doc.text('2.2', {continued: true}).text('Para Pihak sepakat atas hal-hal sebagai berikut:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('2.2', {continued: true}).text('Para Pihak sepakat atas hal-hal sebagai berikut:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('2.2', {continued: true}).text('Para Pihak sepakat atas hal-hal sebagai berikut:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('2.2', {continued: true}).text('Para Pihak sepakat atas hal-hal sebagai berikut:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('2.2', {continued: true}).text('Para Pihak sepakat atas hal-hal sebagai berikut:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('2.2', {continued: true}).text('Para Pihak sepakat atas hal-hal sebagai berikut:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('2.2', {continued: true}).text('Para Pihak sepakat atas hal-hal sebagai berikut:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('2.2', {continued: true}).text('Para Pihak sepakat atas hal-hal sebagai berikut:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('2.2', {continued: true}).text('Para Pihak sepakat atas hal-hal sebagai berikut:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('2.2', {continued: true}).text('Para Pihak sepakat atas hal-hal sebagai berikut:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('2.2', {continued: true}).text('Para Pihak sepakat atas hal-hal sebagai berikut:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('2.2', {continued: true}).text('Para Pihak sepakat atas hal-hal sebagai berikut:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('2.2', {continued: true}).text('Para Pihak sepakat atas hal-hal sebagai berikut:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.text('2.2', {continued: true}).text('Para Pihak sepakat atas hal-hal sebagai berikut:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.pipe(fs.createWriteStream(pnIssuaceFilename))
  doc.end()
  stream.on('finish', function () {
    var result = {message: 'pdf successfully generate', success: true}
    cb(result)
  })
}

function pnIssuanceUber (data, cb) {
  var doc = new Pdf()
  var stream = doc.pipe(blobStream())
  var dir = 'generate_pdf/'
  var bold = 'Times-Bold'
  var italic = 'Times-Italic'
  var bitalic = 'Times-BoldItalic'
  var normal = 'Times-Roman'
  var pnIssuaceFilename = dir + 'PN_ISSUANCE_AGREEMENT(UBER)_' + data.name + '.pdf'
  doc.fontSize(12).font(bold).text('PERJANJIAN PENERBITAN SURAT HUTANG ("Perjanjian") ', 70, 90, {continued: true}).font(normal).text('ini dibuat pada tanggal 2016, antara:', 70, 90).moveDown()
  doc.text('1', 70, 130).text('Jonathan, Warga Negara Indonesia, pemegang Kartu Tanda Penduduk No. , yang berdomisili di (“Penerbit”)', 98, 130, {align: 'justify'}).moveDown(0.5)
  doc.text('2', 70, 160).text('PT Indonusa Bara Sejahtera, suatu perseroan terbatas yang didirikan dan tunduk berdasarkan Hukum yang Berlaku di Indonesia, yang berdomisili di Jalan Panjang (“Agen Fasilitas”) dan', 98, 160, {align: 'justify'}).moveDown(0.5)
  doc.text('3', 70, 210).text('PARA PEMESAN yang tercantum dalam Lampiran 1 dari Perjanjian ini, yang dalam hal ini telah memberikan kuasa kepada Agen Fasilitas untuk melakukan pemesanan atas Surat Hutang (“Pemesan”),', 98, 210, {align: 'justify'}).moveDown(0.5)
  doc.text('Penerbit, Agen Fasilitas dan Pemesan dapat disebut secara bersama-sama sebagai “Para Pihak” dan masing-masing sebagai “Pihak”.', 70, 260).moveDown()

  doc.font(bold).text('LATAR BELAKANG', 70, 310).font(normal).moveDown(0.5)
  doc.text('(A)', 70, 330).text('Penerbit bermaksud untuk menerbitkan dan menawarkan suatu surat hutang kepada Pemesan dalam jumlah sebesar sebagaimana tercantum di Lampiran 2 Perjanjian ini, yang mana bukti surat hutang tersebut adalah sertifikat surat hutang yang dibuat dengan bentuk sebagaimana tersirat di Lampiran 3 Perjanjian ini ("Surat Hutang") kepada Pemesan dengan tunduk ketentuan Perjanjian ini.', 98, 330, {align: 'justify'}).moveDown(0.5)
  doc.text('(B)', 70, 400).text('Sehubungan dengan penerbitan dan pemesanan Surat Hutang yang dilakukan dari Penerbit kepada Pemesan, Pemesan dengan ini setuju untuk memesan dan membayar Surat Hutang dengan menunjuk Agen Fasilitas untuk dan atas nama Pemesan melakukan segala tindakan yang diperlukan sehubungan dengan memfasilitasi pemesanan, penerbitan, dan hal-hal administrasi lainnya sehubungan dengan Surat Hutang sesuai dengan ketentuan yang diatur di dalam Perjanjian ini.', 98, 400, {align: 'justify'}).moveDown(0.5)
  doc.text('(C)', 70, 490).text('Pembagian hak dan kewajiban diantara Pemesan atas Surat Hutang yang diterbitkan adalah secara proporsional sesuai dengan besaran partisipasi tiap-tiap Pemesan sebagaimana tercantum di Lampiran 1 Perjanjian ini.', 98, 490, {align: 'justify'}).moveDown(0.5)
  doc.font(bold).text('OLEH KARENA ITU, TELAH DISEPAKATI SEBAGAI BERIKUT:', 70, 540).moveDown(0.5)
  doc.text('1.', 70, 560).text('DEFINISI DAN INTERPRETASI', 98, 560, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.text('1.1.', 70, 580).text('Dalam Perjanjian ini, selain dinyatakan lain secara tegas:', 98, 580, {align: 'justify'}).moveDown(0.5)
  doc.font(bold).text('“Angsuran” ', 98, 600, {continued: true, align: 'justify'}).font(normal).text('adalah Kewajiban yang Terhutang, yang harus dibayar secara berkala oleh Penerbit kepada Pemesan melalui Agen Fasilitas sesuai dengan Jadwal Pembayaran Angsuran melalui pembayaran tunai terhadap Rekening Agen Fasilitas.').moveDown(0.5)
  doc.font(bold).text('“Entitas” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti setiap individu, perusahaan/korporasi, perseroan terbatas, kemitraan umum (general partnership), kemitraan terbatas (limited partnership), usaha perseorangan, bentuk organisasi bisnis lainnya, wali amanat (trust), serikat, asosiasi, atau Otoritas Pemerintahan.').moveDown(0.5)
  doc.addPage({margin: {top: 40,
      bottom: 50,
      left: 72,
      right: 72}})
  doc.font(bold).text('“Harga Penerbitan” ', 98, 70, {continued: true, align: 'justify'}).font(normal).text('berarti nominal jumlah hutang yang tercantum di Surat Hutang yang diterbitkan sesuai dengan bentuk sebagaimana tersirat di Lampiran 3 Perjanjian ini.').moveDown(0.5)
  doc.font(bold).text('“Hari Kerja” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti setiap hari.').moveDown(0.5)
  doc.font(bold).text('“Hukum yang Berlaku” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti setiap statuta, undang-undang, regulasi, peraturan (termasuk peraturan pencatatan di bursa efek), Perintah, dan bentuk aturan lainnya yang diakui sebagai hukum yang secara sah berlaku dalam suatu yurisdiksi.').moveDown(0.5)
  doc.font(bold).text('“Jadwal Pembayaran Angsuran” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('jadwal pembayaran Angsuran dari Penerbit yang jatuh pada Tanggal Pembayaran Angsuran, yang rinciannya disebutkan pada Lampiran 2 Perjanjian ini.').moveDown(0.5)
  doc.font(bold).text('“Kerugian” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti setiap bentuk kerusakan dan kerugian yang diderita dan/atau ditanggung secara aktual oleh suatu Entitas.').moveDown(0.5)
  doc.font(bold).text('“Kewajiban” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti segala bentuk kewajiban, klaim, kerugian, kerusakan, komitmen, dan tanggung jawab, baik yang diketahui maupun yang tidak diketahui, yang kontingen ', {continued: true}).font(italic).text('(contigent) ', {continued: true}).font(normal).text('maupun  yang tidak, dari suatu Entitas, berdasarkan atau sehubungan dengan antara lain: perjanjian-perjanjian, Pajak, hutang, dan kewajiban pembayaran yang masih terhutang yang berhubungan dengan kegiatan usaha Entitas tersebut.').moveDown(0.5)
  doc.font(bold).text('“Kewajiban yang Terhutang” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti seluruh Kewajiban yang ada pada saat ini dan di kemudian hari termasuk tetapi tidak terbatas pada suatu jumlah (baik jumlah pokok Surat Hutang, bunga, biaya dan hal-hal lainnya) yang terhutang oleh Penerbit kepada Pemesan.').moveDown(0.5)
  doc.font(bold).text('“Otoritas Pemerintahan” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti pemerintah dari negara apapun atau subdivisi politik lainnya (baik pada tingkat pusat, negara bagian, maupun daerah), yang meliputi: kementerian, agensi, otoritas, badan pengatur, pengadilan, bank sentral atau entitas lainnya yang menjalankan kewenangan di bidang eksekutif, legislatif, yudisial, Perpajakan, moneter, atau administratif, atau menjalankan fungsi yang terkait dengan kegiatan pemerintah sebagaimana diatur dalam Hukum yang Berlaku dari negara yang relevan.').moveDown(0.5)
  doc.font(bold).text('“Pajak” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti segala bentuk perpajakan, pajak kekayaan, pengurangan, pajak potongan ', {continued: true}).font(italic).text('(wittholding)', {continued: true}).font(normal).text(', bea, pembebanan, pungutan, biaya, ongkos, kontribusi jaminan sosial, pendapatan modal, PPN, dan suku bunga yang dikenakan, dipungut, ditagih, dipotong atau dinilai oleh Otoritas Pemerintahan di Indonesia atau dimanapun dan bunga, pajak tambahan, penalti, biaya tambahan atau denda yang terkait dan "Perpajakan" akan diartikan dengan merujuk ke istilah Pajak.').moveDown(0.5)
  doc.font(bold).text('“Penutupan” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti penutupan pelaksanaan Pemesanan Surat Hutang sebagaimana diatur dalam Perjanjian ini.').moveDown(0.5)
  doc.font(bold).text('“Perintah” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti setiap bentuk surat perintah, putusan (termasuk putusan yang melarang atau mewajibkan pihak untuk melakukan suatu tindakan (injunction)), keputusan, atau perintah lainnya yang sejenis dari Otoritas Pemerintahan (baik yang bersifat permulaan maupun final).').moveDown(0.5)
  doc.font(bold).text('“Perkara” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti setiap (i) sengketa, tindakan, gugatan, atau perkara yang dilakukan melalui atau di luar pengadilan, majelis arbitrase, atau alternatif penyelesaian sengketa lainnya, atau (ii) investigasi atau audit yang dilakukan oleh Otoritas Pemerintahan.').moveDown(0.5)

  doc.addPage({margin: {top: 40,
      bottom: 50,
      left: 72,
      right: 72}})
  doc.font(bold).text('“Rekening Uber” ', 98, 70, {continued: true, align: 'justify'}).font(normal).text('berarti rekening Penerbit yang dibuka pada Uber yang digunakan sebagai rekening pembagian hasil Penerbit dalam hubungan kemitraan dengan Uber, yang akan dijadikan salah satu sumber pembiayaan dari pembayaran Angsuran dan/atau Kewajiban yang Terhutang.').moveDown(0.5)
  doc.font(bold).text('“Rp” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti Rupiah Indonesia, mata uang Republik Indonesia yang sah.').moveDown(0.5)
  doc.font(bold).text('“Sertifikat Surat Hutang” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti sertifikat yang diterbitkan oleh Penerbit kepada Pemesan atas nama Agen Fasilitas (yang dalam kapasitasnya akan memegang Sertifikat Surat Hutang tersebut untuk dan atas kepentingan dari Agen Fasilitas) yang merupakan bagian yang tidak terpisahkan dari Perjanjian ini.').moveDown(0.5)
  doc.font(bold).text('“Surat Pemberitahuan Wanprestasi” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('memiliki arti sebagaimana didefinisikan dalam Pasal 9.2.').moveDown(0.5)
  doc.font(bold).text('“Tanggal Pembayaran Angsuran” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti tanggal dimana suatu Angsuran menjadi jatuh tempo.').moveDown(0.5)
  doc.font(bold).text('“Tanggal Penutupan” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti tanggal yang jatuh pada selambat-lambatnya 2 Hari Kerja setelah terpenuhinya seluruh persyaratan Pemesanan Surat Hutang sebagaimana dimaksud dalam Pasal 4.1 (kecuali ada yang dikesampingkan oleh Pemesan), atau pada tanggal lain sebagaimana disetujui secara tertulis oleh Para Pihak.').moveDown(0.5)
  doc.font(bold).text('“Uber” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti Uber B.V, suatu korporasi yang didirikan berdasarkan hukum Belanda, yang merupakan pihak mitra dari Penerbit dalam menjalankan kegiatan usahanya.').moveDown(0.5)
  doc.text('1.2.', 70, 370).text('Setiap rujukan-rujukan, tersurat maupun tersirat, atas Hukum yang Berlaku meliputi seluruh legislasi yang didelegasikan dari Hukum yang Berlaku tersebut dan segala perubahan, konsolidasi, penggantian, atau pengundangan kembali dari Hukum yang Berlaku tersebut.', 98, 370, {align: 'justify'}).moveDown(0.5)
  doc.text('1.3.', 70, 430).text('Referensi dalam perjanjian ini untuk Latar Belakang, Pasal-Pasal dan Lampiran-Lampiran adalah referensi terhadap Latar Belakang, Pasal-Pasal dan Lampiran-Lampiran dari Perjanjian ini (kecuali dinyatakan sebaliknya secara tegas). Lampiran-Lampiran Perjanjian ini merupakan bagian yang tidak terpisahkan dari Perjanjian ini.', 98, 430, {align: 'justify'}).moveDown(0.5)
  doc.text('1.4.', 70, 490).text('Judul-Judul dimasukkan hanya untuk kenyamanan pembacaan dan tidak akan mempengaruhi penafsiran dari Perjanjian ini.', 98, 490, {align: 'justify'}).moveDown(0.5)
  doc.text('1.5.', 70, 530).text('Kapanpun Perjanjian ini merujuk pada suatu tanggal, tanggal tersebut akan merujuk pada hari kalendar Gregorian kecuali dinyatakan sebagai Hari Kerja. Rujukan kepada bulan dan tahun juga merujuk kepada kalender Gregorian.', 98, 530, {align: 'justify'}).moveDown(0.5)
  doc.text('1.6.', 70, 580).text('Istilah "Para Pihak" dan "Pihak", sepanjang konteksnya memungkinkan, meliputi setiap penerusnya yang sah atau pihak penerima pengalihan yang diizinkan berdasarkan Perjanjian ini dan Entitas lainnya yang berhak berdasarkan Perjanjian ini atau Hukum yang Berlaku.', 98, 580, {align: 'justify'}).moveDown(0.5)
  doc.addPage({margin: {top: 40,
      bottom: 50,
      left: 72,
      right: 72}})
  doc.font(bold).text('2.', 70, 70).text('PENERBITAN DAN PEMESANAN SURAT HUTANG', 105, 70, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.text('2.1.', 70, 90).text('Dengan tunduk pada ketentuan dan persyaratan dalam Perjanjian ini, Penerbit dengan ini setuju untuk menerbitkan Surat Hutang, dan Pemesan telah sepakat untuk memesan Surat Hutang tersebut.', 105, 90, {align: 'justify'}).moveDown(0.5)
  doc.text('2.2.', 70, 135).text('Para Pihak sepakat atas hal-hal sebagai berikut:', 105, 135, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', 105, 160).text('Untuk tujuan pemesanan Surat Hutang yang akan diterbitkan Penerbit, Pemesan (melalui Agen Fasilitas) akan melakukan pembayaran kepada Penerbit sesuai dengan Harga Penerbitan.', 140, 160, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', 105, 210).text('Bersamaan dengan diterimanya pelunasan pembayaran atas Harga Penerbitan oleh Penerbit, Penerbit wajib menerbitkan Surat Hutang kepada Pemesan dengan cara memberikan Sertifikat Surat Hutang kepada Pemesan. Untuk keperluan penerbitan Sertifikat Surat Hutang tersebut, Pemesan sepakat bahwa Sertifikat Surat Hutang tersebut dapat diterbitkan oleh Penerbit kepada Agen Fasilitas yang akan memegang Sertifikat Surat Hutang Tersebut untuk dan atas kepentingan dari Pemesan.', 140, 210, {align: 'justify'}).moveDown(0.5)
  doc.text('(c)', 105, 310).text('Dana yang diperoleh dari penerbitan Surat Hutang wajib digunakan oleh Penerbit untuk keperluan sebagaimana yang sebelumnya telah disetujui oleh Pemesan.', 140, 310, {align: 'justify'}).moveDown(0.5)
  doc.text('(d)', 105, 340).text('Baik Pemesan maupun Agen Fasilitas tidak terikat untuk memantau atau memeriksa penggunaan jumlah dana yang dipinjam berdasarkan Perjanjian ini.', 140, 340, {align: 'justify'}).moveDown(0.5)
  doc.font(bold).text('3.', 70, 380).text('SURAT HUTANG', 105, 380, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.text('3.1.', 70, 400).text('Surat Hutang yang diterbitkan oleh Penerbit memiliki nilai pokok sebesar sebagaimana tercantum di dalam Surat Hutang tersebut.', 105, 400, {align: 'justify'}).moveDown(0.5)
  doc.text('3.2.', 70, 435).text('Surat Hutang ini diterbitkan untuk berlaku selama masa periode tertentu sebagaimana juga tercantum di dalam Surat Hutang dan sebagai bentuk pelunasan dari Kewajiban yang Terhutang, Penerbit berkewajiban untuk melakukan pembayaran atas Angsuran dan tidak dapat meminjam kembali setiap bagian dari Kewajiban yang Terhutang yang telah dibayarkan kembali kepada Pemesan. Sehubungan dengan hal ini, Para Pihak sepakat bahwa Kewajiban pembayaran atas Angsuran tersebut dilakukan wajib dilakukan oleh Penerbit kepada Agen Pemesan yang dalam kapasitasnya sebagai perwakilan dari Pemesan dalam mengatur pelaksanaan pembayaran tersebut. Pembayaran atas Angsuran akan dilakukan dalam tahapan sebagai berikut:', 105, 435, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', 105, 565).text('dilakukan dengan mekanisme autodebet dari Rekening Uber; dan', 140, 565, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', 105, 585).text('dalam hal proses autodebet dari Rekening Uber tidak cukup untuk pembayaran suatu Angsuran pada suatu Tanggal Pembayaran Angsuran, maka Penerbit berkewajiban untuk melakukan transfer atas sisa Angsuran yang harus dibayarkan kepada Agen Fasilitas paling lambat 1 Hari Kerja setelah Tanggal Pembayaran Angsuran tersebut, pembayaran mana harus dilakukan dengan tunai kepada rekening berikut ini:', 140, 585, {align: 'justify'}).moveDown(0.5)
  doc.addPage({margin: {top: 40,
      bottom: 50,
      left: 72,
      right: 72}})
  doc.font(normal).text('Nama Pemegang Rekening: PT. Indonusa Bara Sejahtera', 140, 70, {align: 'justify'}).moveDown(0.5)
  doc.text('Nomor Rekening: 548-5077798 IDR', 140, 90, {align: 'justify'}).moveDown(0.5)
  doc.text('Nama Bank: BCA KCP Central Park', 140, 110, {align: 'justify'}).moveDown(0.5)
  doc.font(bold).text('("Rekening Agen Fasilitas")', 140, 130, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.text('3.3.', 70, 150).text('Surat Hutang ini diterbitkan dengan tingkat suku bunga flat tetap sebagaimana tercantum di Surat Hutang tersebut, yang akan dibayarkan secara berkala, dan dihitung dan wajib dibayarkan sebagai bagian dari Angsuran. Dalam hal bunga Surat Hutang ini dianggap sebagai pendapatan bunga oleh Otoritas Pemerintah yang berwenang (termasuk diantaranya Direktorat Jenderal Pajak), maka kewajiban atas pelaporan kewajiban Pajak tahunan Pemesan adalah menjadi tanggung jawab Pemesan itu sendiri.', 105, 150, {align: 'justify'}).moveDown(0.5)
  doc.text('3.4.', 70, 255).text('Para Pihak sepakat bahwa:', 105, 255, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', 105, 275).text('Dana yang diterima oleh Penerbit dari hasil penerbitan Surat Hutang wajib digunakan oleh Penerbit khusus sebagaimana tercantum di Pasal 2.2 butir c.', 140, 275, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', 105, 325).text('Penerbit menyetujui dan mengakui bahwa kegagalan Penerbit untuk mematuhi Kewajibannya dalam Pasal 3.4 (a) tidak akan mempengaruhi keabsahan dari ketentuan Perjanjian ini dalam bentuk apa pun.', 140, 325, {align: 'justify'}).moveDown(0.5)
  doc.text('3.5.', 70, 375).text('Pada Tanggal Penutupan:', 105, 375, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', 105, 395).text('Penerbit wajib menyampaikan kepada Agen Fasilitas dokumen asli dan salinan atas bukti-bukti yang menunjukkan bahwa seluruh persyaratan sebagaimana diatur dalam Pasal 4.1 telah terpenuhi dimana bukti-bukti tersebut harus dalam bentuk yang dapat diterima oleh Agen Fasilitas.', 140, 395, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', 105, 455).text('Penerbit wajib melakukan hal-hal sebagai berikut:', 140, 455, {align: 'justify'}).moveDown(0.5)
  doc.text('(i)', 140, 475).text('menyampaikan informasi kepada Agen Fasilitas mengenai rekening bank Penerbit yang akan menerima pembayaran Harga Penerbitan', 180, 475).font(bold).text('(“Rekening yang Ditunjuk”)', {continued: true}).font(normal).text('; dan', {align: 'justify'}).moveDown(0.5)
  doc.text('(ii)', 140, 525).text('menyampaikan dokumen-dokumen lainnya sebagaimana diminta secara wajar oleh kepada Agen Fasilitas dan/atau Pemesan untuk memastikan pemenuhan Kewajiban Penerbit berdasarkan ketentuan Perjanjian ini.', 180, 525, {align: 'justify'}).moveDown(0.5)
  doc.text('(c)', 105, 590).text('Pemesan, melalui Agen Fasilitas, akan menyetorkan komitmen dananya untuk melaksanakan pemesanan Surat Hutang dengan cara melakukan transfer dana senilai Harga Penerbitan ke Rekening yang Ditunjuk setelah ketentuan dalam Pasal 3.5 (a) dan (b) dipenuhi seluruhnya.', 140, 590, {align: 'justify'}).moveDown(0.5)
  doc.text('(d)', 105, 655).text(' Bersamaan dengan dilaksanakannya penyetoran dana sebagaimana dimaksud dalam Pasal 3.5 (c), Penerbit wajib menyerahkan kepada Agen Fasilitas, Sertifikat Surat Hutang yang membuktikan kepemilikan Pemesan atas Surat Hutang, yang isinya sesuai dengan bentuk yang ditetapkan dalam Lampiran 3.', 140, 655, {align: 'justify'}).moveDown(0.5)
  doc.addPage({margin: {top: 40,
      bottom: 50,
      left: 72,
      right: 72}})
  doc.font(bold).text('4.', 70, 70).text('PERSYARATAN-PERSYARATAN', 105, 70, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.text('4.1.', 70, 90).text('Agar Pemesanan Surat Hutang dapat dilaksanakan, persyaratan-persyaratan di bawah ini wajib dipenuhi terlebih dahulu:', 105, 90, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', 105, 125).text('Pemesan telah mendapatkan seluruh Persetujuan yang dibutuhkan untuk menandatangani Perjanjian ini dan melaksanakan seluruh Kewajibannya berdasarkan Perjanjian ini;', 140, 125, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', 105, 170).text('seluruh pernyataan dan jaminan berdasarkan Perjanjian ini masih tetap akurat, benar dan tidak menyesatkan baik pada tanggal Perjanjian ini maupun Tanggal Penutupan;', 140, 170, {align: 'justify'}).moveDown(0.5)
  doc.text('(c)', 105, 215).text('Pemesan telah menandatangani perjanjian dan surat kuasa bermeterai untuk pemotongan rekening (autodebet dari Rekening Uber) dengan Uber, dalam bentuk yang dapat diterima oleh Agen Fasilitas ;', 140, 215, {align: 'justify'}).moveDown(0.5)
  doc.text('(d)', 105, 260).text('seluruh janji-janji dan Kewajiban yang disyaratkan oleh Pemesan dan/atau Agen Fasilitas untuk dilaksanakan atau patuhi oleh Penerbit berdasarkan Perjanjian ini pada atau sebelum Tanggal Penutupan harus telah dilaksanakan dan dipatuhi seluruhnya;', 140, 260, {align: 'justify'}).moveDown(0.5)
  doc.text('(e)', 105, 320).text('tidak terjadi keadaan wanprestasi sebagaimana diatur dalam Pasal 9.1; dan', 140, 320, {align: 'justify'}).moveDown(0.5)
  doc.text('(f)', 105, 340).text('penerbitan dan pemesanan Surat Hutang tidak dilarang berdasarkan Hukum yang Berlaku.', 140, 340, {align: 'justify'}).moveDown(0.5)
  doc.text('4.2.', 70, 370).text('Jika setiap persyaratan yang diatur dalam Pasal 4.1 tidak terpenuhi sampai dengan jangka waktu 1 minggu sejak tanggal Perjanjian ini atau tanggal lain yang disepakati oleh Pemesan dan Penerbit, Perjanjian ini akan segera berakhir dan dalam keadaan tersebut Para Pihak akan dilepaskan dan dibebaskan dari Kewajiban mereka masing-masing berdasarkan Perjanjian ini dan tidak ada Pihak yang akan memiliki klaim apapun terhadap Pihak lain.', 105, 370, {align: 'justify'}).moveDown(0.5)
  doc.font(bold).text('5.', 70, 455).text('PEMBAYARAN LEBIH AWAL', 105, 455, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.text('5.1.', 70, 475).text('Jika, setiap saat berdasarkan Hukum Yang Berlaku, Pemesan menjadi tidak sah untuk melakukan salah satu kewajibannya sebagaimana dimaksud dalam Perjanjian ini, termasuk namun tidak terbatas untuk membeli Surat Hutang dari Penerbit, maka', 105, 475, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', 105, 520).text('Pemesan harus segera memberitahukan kepada Agen Fasilitas setelah mengetahui hal tersebut', 140, 520, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', 105, 550).text('Agen Fasilitas harus segera memberitahukan hal tersebut kepada Penerbit.', 140, 550, {align: 'justify'}).moveDown(0.5)
  doc.text('(c)', 105, 570).text('Penerbit harus segera melunasi Kewajiban yang Terhutang segera setelah menerima pemberitahuan dari Agen Fasilitas sebagaimana dimaksud di dalam Pasal 5.1 (ii) di atas.', 140, 570, {align: 'justify'}).moveDown(0.5)
  doc.text('5.2.', 70, 620).text('Dalam hal Penerbit menghentikan hubungan kemitraannya dengan Uber sebelum seluruh Kewajiban yang Terhutang telah selesai dilunasi oleh Penerbit berdasarkan ketentuan yang diatur di dalam Perjanjian ini, maka seluruh Kewajiban yang Terhutang akan segera menjadi jatuh tempo pada saat berhentinya hubungan kemitraan tersebut dan pada hari yang sama, Penerbit wajib melunasi seluruh Kewajiban Terhutang tersebut kepada Pemesan, melalui Agen Fasilitas.', 105, 620, {align: 'justify'}).moveDown(0.5)
  doc.addPage({margin: {top: 40,
      bottom: 50,
      left: 72,
      right: 72}})
  doc.font(normal).text('5.3.', 70, 70).text('Penerbit setiap waktu diperbolehkan untuk membayar kembali seluruh sisa Kewajiban yang Terhutang sebelum berakhir jangka waktunya, dengan ketentuan Penerbit telah menyampaikan surat pemberitahuan tertulis kepada Agen Fasilitas selambat-lambatnya 7 Hari Kerja sebelumnya tentang kehendak untuk melakukan pembayaran kembali yang dipercepat, yang dengan tegas menyebutkan jumlah seluruh sisa Kewajiban yang Terhutang yang akan dilunasi dan kapan akan dilaksanakan pembayaran tersebut, dengan ketentuan pemberitahuan tertulis tersebut tidak dapat ditarik kembali oleh Penerbit.', 105, 70, {align: 'justify'}).moveDown(0.5)
  doc.font(bold).text('6.', 70, 170).text('DENDA', 105, 170, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.text('6.1.', 70, 190).text('Dalam hal Penerbit lalai untuk membayar Kewajibannya berdasarkan Perjanjian, baik Angsuran, sisa Kewajiban yang Terhutang, biaya-biaya dan/atau suatu jumlah lain yang terutang dan wajib dibayar oleh Penerbit kepada Pemesan melalui Agen Fasilitas karena sebab apapun pada tanggal jatuh waktunya sebagaimana ditentukan dalam Perjanjian, maka Penerbit wajib dan setuju untuk membayar denda sebesar [0,1% per hari yang dihitung dari Angsuran] untuk setiap hari keterlambatan.', 105, 190, {align: 'justify'}).moveDown(0.5)
  doc.text('6.2.', 70, 275).text('Pengenaan denda dihitung sejak tanggal jatuh waktu suatu pembayaran sampai dengan tanggal pelunasan Kewajiban tersebut. Denda tersebut harus dibayar secara seketika dan sekaligus lunas bersama dengan pembayaran yang telah lewat waktu dimaksud.', 105, 275, {align: 'justify'}).moveDown(0.5)
  doc.text('6.3.', 70, 320).text('Perhitungan denda tersebut dalam Pasal 6.1 Perjanjian ini dilakukan secara harian atas dasar pembagi tetap 30 hari dalam sebulan.', 105, 320, {align: 'justify'}).moveDown(0.5)
  doc.font(bold).text('7.', 70, 355).text('PERNYATAAN', 105, 355, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.text('7.1.', 70, 375).text('Penerbit menyatakan kepada Pemesan bahwa terhitung sejak tanggal Perjanjian ini, pernyataan dan informasi sebagaimana dinyatakan di bawah ini adalah benar, tepat dan tidak menyesatkan secara material ', 105, 375, {align: 'justify', continued: true}).font(bold).text('("Pernyataan Penerbit"):').font(normal).moveDown(0.5)
  doc.text('(a)', 105, 420).text('Penerbit memiliki kekuasaan untuk memiliki asetnya dan memiliki kekuasaan penuh, wewenang dan  hak hukum untuk menerbitkan Surat Hutang dan menandatangani Perjanjian ini.', 140, 420, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', 105, 465).text('Penerbit tidak dalam proses kepailitan, penundaan Kewajiban pembayaran hutang atau proses-proses semacamnya dan tidak ada permohonan untuk kepailitan, penundaan Kewajiban pembayaran hutang atau proses semacamnya yang sedang dilakukan.', 140, 465, {align: 'justify'}).moveDown(0.5)
  doc.text('(c)', 105, 520).text('Perjanjian ini dan Surat Hutang dianggap sah, berlaku dan mengikat Kewajiban Penerbit dapat dilaksanakan sesuai dengan ketentuan-ketentuannya.', 140, 520, {align: 'justify'}).moveDown(0.5)
  doc.text('(d)', 105, 550).text('Penandatanganan Perjanjian ini dan penerbitan Surat Hutang untuk kepentingan Penerbit.', 140, 550, {align: 'justify'}).moveDown(0.5)
  doc.text('(e)', 105, 580).text('seluruh informasi yang diberikan dan dokumen-dokumen yang dikirimkan kepada Pemesan dan Agen Fasilitas oleh atau atas nama Penerbit adalah benar dan akurat dalam semua hal.', 140, 580, {align: 'justify'}).moveDown(0.5)
  doc.addPage({margin: {top: 40,
      bottom: 50,
      left: 72,
      right: 72}})
  doc.font(normal).text('(f)', 105, 70).text('Penerbit umumnya tunduk pada hukum perdata, hukum usaha dan atas Perkara hukum, dan baik Penerbit maupun asetnya atau pendapatannya tidak berhak atas hak kekebalan atau hak istimewa (berdaulat atau sebaliknya) dari setiap penjumpaan utang (set-off), pemutusan, eksekusi, sita jaminan atau proses hukum lainnya.', 140, 70, {align: 'justify'}).moveDown(0.5)
  doc.text('7.2.', 70, 145).text('Penerbit mengakui bahwa Pemesan mengikat Perjanjian ini dengan mengandalkan kebenaran dan keakuratan Pernyataan Penerbit.', 105, 145, {align: 'justify'}).moveDown(0.5)
  doc.font(bold).text('8.', 70, 180).text('JANJI-JANJI', 105, 180, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.text('8.1.', 70, 200).text('Selain dari Kewajiban lainnya yang diatur dalam Perjanjian ini, sejak tanggal Perjanjian ini sampai dengan pelunasan penuh Kewajiban yang Terhutang, Penerbit sepakat untuk mematuhi dan melaksanakan seluruh Kewajibannya sebagaimana dinyatakan di bawah ini.', 105, 200, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', 105, 250).text('Penerbit tidak akan mengajukan, dan tidak akan memberikan persetujuan kepada suami atau istrinya untuk mengajukan, permohonan kepailitan atau penundaan Kewajiban pembayaran hutang atau prosedur serupa lainnya atas dirinya sendiri atau atas  suami atau istrinya.', 140, 250, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', 105, 310).text('Penerbit tidak akan menjual atau dengan cara lain mengalihkan, membagikan atau melepaskan semua atau sebagian usahanya atau asset atau pendapatannya, baik dalam satu atau beberapa transaksi yang terkait maupun tidak tidak terkait, yang mungkin mempengaruhi kemampuan Penerbit untuk melakukan pelunasan atas Kewajiban yang Terhutang.', 140, 310, {align: 'justify'}).moveDown(0.5)
  doc.text('(c)', 105, 385).text('Penerbit tidak akan mengadakan perjanjian atau kewajiban lainnya yang mungkin menyebabkan Penerbit tidak mampu untuk melakukan pelunasan atas Kewajiban yang Terhutang.', 140, 385, {align: 'justify'}).moveDown(0.5)
  doc.text('(d)', 105, 430).text('Penerbit harus memastikan Kewajibannya berdasarkan Perjanjian ini setiap saat paling tidak pari passu dengan utang Penerbit lainnya.', 140, 430, {align: 'justify'}).moveDown(0.5)
  doc.text('(e)', 105, 465).text('Penerbit harus tepat waktu membayar semua Kewajiban yang Terhutang yang harus dilunasi sesuai dengan ketentuan yang diatur di dalan Perjanjian ini.', 140, 465, {align: 'justify'}).moveDown(0.5)
  doc.font(bold).text('9.', 70, 500).text('KEADAAN WANPRESTASI', 105, 500, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.text('9.1.', 70, 520).text('Salah satu atau lebih dari kejadian-kejadian atau hal-hal-tersebut di bawah ini akan dianggap sebagai wanprestasi dari Penerbit apabila terjadi sebelum dilunasinya Kewajiban yang Terhutang secara keseluruhan. ', 105, 520, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', 105, 570).text('Penerbit tidak melakukan pembayaran atas seluruh maupun sebagian Kewajiban yang Terhutang yang telah jatuh tempo dan harus dibayar atau terhutang pada saat jatuh tempo sesuai dengan ketentuan-ketentuan dalam Perjanjian ini kecuali kelalaian tersebut disebabkan hal administratif dan teknis, dan pembayaran akhirnya terlaksana dalam waktu 2 Hari Kerja setelah tanggal jatuh tempo yang relevan;', 140, 570, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', 105, 660).text('Setiap pernyataan atau jaminan yang dibuat oleh Penerbit berdasarkan Pasal 7 ternyata tidak benar, secara material, tidak sesuai dengan kenyataan sebenarnya atau menyesatkan, atau terjadi pelanggaran atas janji-janji berdasarkan Pasal 8 (baik janji untuk melakukan atau janji untuk tidak melakukan) sebagaimana disyaratkan dalam Perjanjian ini dan jika keadaan tersebut dapat diperbaiki, hal itu tidak diperbaiki dalam waktu 5 Hari Kerja setelah Agen Fasilitas menyampaikan pemberitahuan kepada Penerbit; atau', 140, 660, {align: 'justify'}).moveDown(0.5)
  doc.text('(c)', 105, 120).text('Penerbit menyatakan secara tertulis dan secara umum tidak dapat membayar utangnya pada tanggal jatuh tempo atau mengajukan Penundaan Kewajiban Pembayaran Utang (PKPU) ke pengadilan niaga, berhenti atau menghentikan sementara pembayaran-pembayaran kepada kreditur-kreditur atau tidak dapat atau mengakui ketidak sanggupannya untuk membayar hutang-hutangnya pada waktu jatuh tempo atau dinyatakan jatuh pailit.', 140, 120, {align: 'justify'}).moveDown(0.5)
  doc.text('9.2.', 70, 210).text('Jika suatu Wanprestasi telah terjadi maka Agen Fasilitas dapat memberikan surat pemberitahuan Wanprestasi ', 105, 210, {align: 'justify', continued: true}).font(bold).text('(“Surat Pemberitahuan Wanprestasi”) ', {continued: true}).font(normal).text('dan setelahnya (kecuali Penerbit dapat memperbaiki Wanprestasi tersebut sesuai dengan jangka waktu yang diatur di dalam Perjanjian ini) maka Agen Fasilitas berhak memberitahukan secara tertulis kepada Penerbit:').moveDown(0.5)
  doc.text('(a)', 105, 280).text('menyatakan jumlah Kewajiban yang Terhutang yang seketika itu jatuh tempo dan harus dibayar oleh Penerbit tanpa pemberitahuan lebih lanjut; dan/atau', 140, 280, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', 105, 310).text('menyatakan Perjanjian ini berakhir tanpa mengurangi hak Pemesan dan Agen Fasilitas untuk menuntut hak-haknya berdasarkan Perjanjian ini.', 140, 310, {align: 'justify'}).moveDown(0.5)
  doc.font(bold).text('10.', 70, 350).text('PENGAKHIRAN', 105, 350, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.text('10.1.', 70, 370).text('Perjanjian ini dapat diakhiri berdasarkan hal-hal sebagai berikut:', 105, 370, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', 105, 390).text('berdasarkan persetujuan tertulis oleh Para Pihak;', 140, 390, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', 105, 410).text('terjadinya keadaan Wanprestasi sebagaimana diatur dalam Pasal 9.1; atau', 140, 410, {align: 'justify'}).moveDown(0.5)
  doc.text('(c)', 105, 430).text('dilunasinya seluruh Kewajiban yang Terhutang oleh Penerbit kepada Pemesan, melalui Agen Fasilitas.', 140, 430, {align: 'justify'}).moveDown(0.5)
  doc.text('10.2.', 70, 470).text('Kecuali dinyatakan sebaliknya dalam Pasal 10.3 dan Pasal 10.4, seluruh hak dan Kewajiban Para Pihak akan berakhir dengan segera setelah pengakhiran Perjanjian ini dan tidak ada Pihak yang harus bertanggung jawab terhadap Pihak lainnya atas Kewajiban-Kewajiban apapun sebagai akibat dari pengakhiran dari Perjanjian.', 105, 470, {align: 'justify'}).moveDown(0.5)
  doc.text('10.3.', 70, 530).text('Pengakhiran Perjanjian ini tidak akan berdampak terhadap hak masing-masing Pihak untuk menerima ganti rugi dari Pihak lainnya sehubungan dengan ketentuan dalam Perjanjian ini sepanjang hak tersebut sudah timbul sebelum atau pada saat Perjanjian ini diakhiri.', 105, 530, {align: 'justify'}).moveDown(0.5)
  doc.text('10.4.', 70, 590).text('Ketentuan dalam Pasal 9.2, 10.2, 10.3, 10.4, 11, 12.10 dan 12.11 akan tetap berlaku setelah Perjanjian ini diakhiri.', 105, 590, {align: 'justify'}).moveDown(0.5)
  doc.text('10.5.', 70, 625).text('Para Pihak dengan tidak dapat ditarik kembali setuju untuk mengesampingkan ketentuan Pasal 1266 dari Kitab Undang-Undang Hukum Perdata sepanjang penetapan pengadilan diperlukan untuk pembatalan atau pengakhiran terlebih dahulu dari Perjanjian ini.', 105, 625, {align: 'justify'}).moveDown(0.5)
  doc.addPage({margin: {top: 40,
      bottom: 50,
      left: 72,
      right: 72}})
  doc.font(bold).text('11.', 70, 70).text('GANTI RUGI', 105, 70, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.text('11.1.', 70, 90).text('Penerbit wajib memberikan ganti rugi kepada Pemesan dan/atau Agen Fasilitas sehubungan dengan: (i) Kerugian yang secara nyata dialami oleh Pemesan dan/atau Agen Fasilitas sebagai akibat kegagalan Penerbit dalam melaksanakan kewajibannya berdasarkan Perjanjian ini (termasuk segala bentuk wanprestasi sebagaimana diatur dalam Pasal 9.1), dan (ii) seluruh biaya sehubungan dengan dokumentasi (termasuk biaya konsultan hukum yang wajar) dan biaya lainnya yang dikeluarkan oleh Pemesan untuk menuntut hak Pemesan dan/atau Agen Fasilitas atas Kerugian sebagaimana disebutkan di atas.', 105, 90, {align: 'justify'}).moveDown(0.5)
  doc.text('11.2.', 70, 205).text('Semua klaim penggantian Kerugian berdasarkan Pasal 11.1 ini harus dibayarkan oleh Penerbit kepada Pemesan dan/atau Agen Fasilitas dalam jangka waktu 5 Hari Kerja setelah pemberitahuan tertulis terhadap klaim-klaim tersebut berikut dengan bukti-bukti pendukung telah disampaikan oleh Pemesan dan/atau Agen Fasilitas kepada Penerbit.', 105, 205, {align: 'justify'}).moveDown(0.5)
  doc.font(bold).text('12.', 70, 270).text('LAIN-LAIN', 105, 270, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.text('12.1.', 70, 290).text('Semua pemberitahuan, permintaan dan bentuk komunikasi lainnya harus dilakukan secara tertulis dalam Bahasa Indonesia dan akan dianggap telah diberikan apabila pemberitahuan tersebut diserahkan secara personal atau dengan transmisi faksimili atau melalui pos (pos prabayar kelas pertama) atau dikirimkan melalui surat elektronik kepada Para Pihak dengan alamat di bawah ini:', 105, 290, {align: 'justify'}).moveDown(0.5)
  doc.text('Apabila kepada Pemesan dan/atau Agen Fasilitas:').moveDown(0.5)
  doc.font(bold).text('PT Indonusa Bara Sejahtera').font(normal).moveDown(0.5)
  doc.text('Alamat', 105, 400).text(':', 170, 400).text('Rukan Golden 8 No.8C Jalan Panjang, Kedoya Utara Jakarta Barat, 11520 Indonesia.', 200, 400).moveDown(0.5)
  doc.text('Telp. No.', 105, 430).text(':', 170, 430).text('021 - 2920095', 200, 430).moveDown(0.5)
  doc.text('Fax. No.', 105, 450).text(':', 170, 450).text('-', 200, 450).moveDown(0.5)
  doc.text('Email', 105, 470).text(':', 170, 470).text('admin@taralite.com', 200, 470).moveDown(0.5)
  doc.text('U.P.', 105, 490).text(':', 170, 490).text('Abraham Viktor', 200, 490).moveDown(0.5)
  doc.text('Apabila kepada Penerbit:', 105, 510).moveDown(0.5)
  doc.text('Alamat', 105, 530).text(':', 170, 530).moveDown(0.5)
  doc.text('No. Telepon', 105, 550).text(':', 170, 550).moveDown(0.5)
  doc.text('Setiap Pihak dari waktu ke waktu dapat mengganti alamat, nomor faksimili atau informasi lainnya dengan tujuan untuk melakukan pemberitahuan-pemberitahuan kepada Pihak lainnya dengan memberikan pemberitahuan terkait perubahan informasi tersebut kepada Pihak lainnya.', 105, 570, {align: 'justify'})
  doc.text('12.2', 70, 640).text('Ketentuan dan kondisi Perjanjian ini dapat dikesampingkan dari waktu ke waktu oleh Para Pihak yang berhak atas pemberian pengesampingan tersebut, tetapi tidak ada pengesampingan yang akan menjadi efektif kecuali pengesampingan tersebut dituangkan dalam instrumen tertulis yang ditandatangani oleh atau atas nama Pihak yang mengesampingkan ketentuan atau kondisi tersebut.', 105, 640, {align: 'justify'}).moveDown(0.5)
  doc.addPage({margin: {top: 40,
      bottom: 50,
      left: 72,
      right: 72}})
  doc.font(normal).text('12.3', 70).text('Perjanjian ini hanya dapat diubah, ditambah, atau dimodifikasi dengan instrumen tertulis yang ditandatangani secara bersama-sama oleh atau atas nama Para Pihak.', 105, 70, {align: 'justify'}).moveDown(0.5)
  doc.text('12.4', 70, 105).text('Tanpa persetujuan tertulis terlebih dahulu dari Penerbit dan cukup dengan diberikannya suatu pemberitahuan secara tertulis dari Agen Fasilitas kepada Penerbit, Pemesan berhak untuk mengalihkan kepada pihak ketiga sebagian atau seluruh hak dan Kewajibannya berdasarkan Perjanjian ini dengan cara apapun dan Penerbit dengan ini memberikan persetujuannya atas hal tersebut. Penerbit berjanji dan mengikat diri untuk atas permintaan pertama Pemesan, menandatangani, membuat dan menyerahkan dokumen-dokumen yang diminta Pemsan dan melaksanakan perjanjian yang dibuat sehubungan dengan pengalihan dimaksud. Perjanjian tetap mengikat para pengganti hak masing-masing pihak. Selanjutnya Penerbit tidak berhak untuk mengalihkan atau memindahkan dengan cara apapun sebagian atau seluruh hak-hak dan Kewajiban-Kewajibannya berdasarkan Perjanjian tanpa persetujuan tertulis terlebih dahulu dari Pemesan.', 105, 105, {align: 'justify'}).moveDown(0.5)
  doc.text('12.5', 70, 260).text('Apabila ada ketentuan dalam Perjanjian ini yang dinyatakan ilegal, tidak sah, atau tidak dapat dilaksanakan berdasarkan Hukum yang Berlaku saat ini atau masa mendatang, dan apabila hak atau Kewajiban Para Pihak berdasarkan Perjanjian ini tidak akan merugikan secara material:', 105, 260, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', 105, 320).text('ketentuan tersebut akan dipisahkan secara sepenuhnya;', 140, 320, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', 105, 340).text('Perjanjian ini akan ditafsirkan dan berlaku seakan-akan ketentuan yang ilegal, tidak sah, atau tidak dapat dilaksanakan tersebut tidak pernah menjadi bagian dari Perjanjian ini; dan', 140, 340, {align: 'justify'}).moveDown(0.5)
  doc.text('(c)', 105, 390).text('ketentuan lainnya dari Perjanjian ini akan tetap berlaku penuh dan tidak akan terpengaruh oleh ketentuan yang ilegal, tidak sah, atau tidak dapat dilaksanakan atau oleh pemisahan dari ketentuan yang bersangkutan.', 140, 390, {align: 'justify'}).moveDown(0.5)
  doc.text('12.6', 70, 440).text('Kecuali diwajibkan berdasarkan peraturan perundang-undangan yang berlaku, Para Pihak setuju untuk tidak mengungkapkan isi Perjanjian kepada pihak ketiga manapun tanpa memperoleh persetujuan terlebih dahulu dari Pihak lainnya.', 105, 440, {align: 'justify'}).moveDown(0.5)
  doc.text('12.7', 70, 490).text('Perjanjian ini dapat ditandatangani dalam beberapa salinan dimana tiap-tiap salinan tersebut dianggap asli dan memiliki keberlakuan yang sama dan salinan-salinan tersebut akan dianggap sebagai satu kesatuan dokumen yang asli dan akan mengikat Para Pihak seakan-akan Perjanjian ini hanya terdiri atas satu dokumen.', 105, 490, {align: 'justify'}).moveDown(0.5)
  doc.text('12.8', 70, 550).text('Perjanjian ini beserta hak-hak dan Kewajiban-Kewajiban yang timbul atau berkenaan dengannya tunduk pada hukum Negara Republik Indonesia.', 105, 550, {align: 'justify'}).moveDown(0.5)
  doc.text('12.9', 70, 580).text('Para pihak dengan ini sepakat bahwa Pengadilan Negeri Jakarta Barat mempunyai yurisdiksi yang non-eksklusif untuk memeriksa dan memutuskan segala gugatan, tuntutan atau tindakan hukum yang timbul berkenaan dengan Perjanjian.', 105, 580, {align: 'justify'}).moveDown(0.5)
  doc.text('12.10', 70, 630).text('Jika setiap Sertifikat Surat Hutang terpotong, rusak, hancur, dicuri atau hilang, sertifikat tersebut dapat diganti oleh Penerbit setelah Agen Fasilitas melakukan pembayaran kepada Penerbit atas biaya yang mungkin dikenakan sehubungan dengan penggantian tersebut, sebagaimana Penerbit dapat minta. Sertifikat Surat Hutang yang rusak atau hancur harus diserahkan kepada Penerbit sebelum penggantinya diterbitkan.', 105, 630, {align: 'justify'}).moveDown(0.5)
  doc.addPage({margin: {top: 40,
      bottom: 50,
      left: 72,
      right: 72}})
  doc.font(normal).text('12.11', 70, 70).text('Para Pihak sepakat bahwa pembayaran yang dilakukan oleh Penerbit secara tunai (baik dalam konteks melakukan pembayaran terhadap Kewajiban yang Terhutang, Angsuran, ganti Kerugian, dan lainnya) wajib ditujukan kepada Rekening Agen Fasilitas.', 105, 70, {align: 'justify'}).moveDown(0.5)
  doc.font(italic).text('[HALAMAN TANDA TANGAN ADA PADA HALAMAN SELANJUTNYA]', 70, null, {align: 'center'}).font(normal).moveDown(0.5)
  doc.addPage({margin: {top: 40,
      bottom: 50,
      left: 72,
      right: 72}})
  doc.font(italic).text('DITANDATANGANI pada tanggal yang pertama kali dituliskan di atas', 70, 70).font(normal).moveDown(0.5)
  doc.font(bold).text('PENERBIT', 70, 90).font(normal).moveDown(0.5)
  doc.font(italic).text('Oleh', 70, 170, {continued: true}).text('                               ', 105, 170, {underline: true}).font(normal).moveDown(0.5)
  doc.font(italic).text('Nama', 70, 190, {continued: true}).font(normal).text(':', 99, 190).moveDown(0.5)
  doc.font(bold).text('PEMESAN', 70, 240).font(normal).moveDown(0.5)
  doc.font(bold).text('dalam hal ini diwakili oleh', 70, 260).font(normal).moveDown(0.5)
  doc.font(bold).text('PT INDONUSA BARA SEJAHTERA', 70, 280).font(normal).moveDown(0.5)
  doc.font(italic).text('Oleh', 70, 360, {continued: true}).text('                               ', 105, 360, {underline: true}).font(normal).moveDown(0.5)
  doc.font(italic).text('Nama', 70, 380, {continued: true}).font(normal).text(':', 99, 380, {continued: true}).text('Abraham Viktor', 110, 380).moveDown(0.5)
  doc.font(italic).text('Jabatan', 70, 400, {continued: true}).font(normal).text(':', 90, 400, {continued: true}).text('Direktur Utama', 100, 400).moveDown(0.5)
  doc.font(bold).text('AGEN FASILITAS', 70, 450).font(normal).moveDown(0.5)
  doc.font(bold).text('PT INDONUSA BARA SEJAHTERA', 70, 470).font(normal).moveDown(0.5)
  doc.font(italic).text('Oleh', 70, 560, {continued: true}).text('                               ', 105, 560, {underline: true}).font(normal).moveDown(0.5)
  doc.font(italic).text('Nama', 70, 580, {continued: true}).font(normal).text(':', 99, 580).moveDown(0.5)
  doc.font(italic).text('Jabatan', 70, 600, {continued: true}).font(normal).text(':', 90, 600).moveDown(0.5)
  doc.addPage({margin: {top: 40,
      bottom: 50,
      left: 72,
      right: 72}})
  doc.font(bold).text('LAMPIRAN 1', 70, 70, {align: 'center'}).font(normal).moveDown(0.5)
  doc.font(bold).text('DAFTAR PEMESAN', 70, 90, {align: 'center'}).font(normal).moveDown(0.5)
  doc.text('No', 70, 140).text('Nama', 98, 140).text('Partisipasi', 395, 140).moveDown(0.5)
  doc.text('1', 70, 160).text('Abraham Viktor', 98, 160).text('Rp.', 395, 160).moveDown(3)
  doc.font(bitalic).text('[Note: Format untuk disesuaikan oleh Taralite]', 70, null, {align: 'left'})

  doc.addPage({margin: {top: 40,
      bottom: 50,
      left: 72,
      right: 72}})
  doc.font(bold).text('LAMPIRAN 2', 70, 70, {align: 'center'}).font(normal).moveDown(0.5)
  doc.font(bold).text('JUMLAH POKOK HUTANG SERTA JADWAL PEMBAYARAN ANGSURAN', 70, 90, {align: 'center'}).font(normal).moveDown(0.5)
  var table = new PdfTable(doc, {
    bottomMargin: 30
  })
  table
  .addPlugin(new (require('voilab-pdf-table/plugins/fitcolumn'))({
    column: 'num'
  }))
  .setColumnsDefaults({
    headerBorder: 'B',
    align: 'right'
  })
  .addColumns([
    {
      id: 'num',
      header: 'Angsuran',
      align: 'left',
      width: 80
    },
    {
      id: 'amount',
      header: 'Jumlah Pembayaran Kewajiban yang Terhutang (termasuk Bunga)',
      align: 'center',
      width: 240
    },
    {
      id: 'date',
      header: 'Tanggal  Pembayaran Angsuran',
      align: 'center',
      width: 150
    }
  ])
  .onPageAdded(function (tb) {
    tb.addHeader()
  })
  table.addBody([
    {num: '1', amount: '', date: ''},
    {num: '2', amount: '', date: ''},
    {num: '3', amount: '', date: ''}
  ])
  doc.addPage({margin: {top: 40,
      bottom: 50,
      left: 72,
      right: 72}})
  doc.font(bold).text('LAMPIRAN 3', 70, 70, {align: 'center'}).font(normal).moveDown(0.5)
  doc.font(bold).text('BENTUK SERTIFIKAT SURAT HUTANG', 70, 90, {align: 'center'}).font(normal).moveDown(0.5)
  doc.font(bold).text('SERTIFIKAT SURAT HUTANG', 70, 130, {align: 'center'}).font(normal).moveDown(0.5)
  doc.font(bold).text('Jumlah Pokok', 70, 150).text(':', 170, 150).text('Rp [  ]', 190, 150).font(normal).moveDown(0.5)
  doc.font(bold).text('Tenor', 70, 170).text(':', 170, 170).text('[  ]  minggu', 190, 170).font(normal).moveDown(0.5)
  doc.font(bold).text('Jenis', 70, 190).text(':', 170, 190).text('angsuran tetap mingguan', 190, 190).font(normal).moveDown(0.5)
  doc.font(bold).text('Bunga (flat)', 70, 210).text(':', 170, 210).text('[  ] % per minggu', 190, 210).font(normal).moveDown(0.5)
  doc.text('No.', 70, 230).text(':', 170, 230).text('[  ]', 190, 230).font(normal).moveDown(0.5)
  doc.text('Tanggal Penerbitan', 70, 250).text(':', 170, 250).text('[  ] 2016', 190, 250).font(normal).moveDown(0.5)
  doc.text('Sertifikat Surat Hutang (', 70, 270, {continued: true}).font(bold).text('"Sertifikat"', {continued: true}).font(normal).text(') ini diterbitkan oleh [  ] (', {continued: true}).font(bold).text('"Penerbit"', {continued: true}).font(normal).text(') dengan syarat-syarat dan ketentuan yang diatur di dalam, dan merupakan bagian yang tidak terpisahkan dari, Perjanjian Penerbitan Surat Hutang tanggal [  ] 2016 (', {continued: true, align: 'justify'}).font(bold).text('"Perjanjian"', {continued: true}).font(normal).text(') antara Penerbit dengan PEMESAN yang terdaftar dalam Lampiran 1 dari Perjanjian (', {continued: true}).font(bold).text('"Pemesan"', {continued: true}).font(normal).text(') dan PT Indonusa Bara Sejahtera selaku Agen Fasilitas yang dalam hal ini telah diberikan kuasa dari Pemesan untuk memegang Surat Hutang untuk dan atas kepentingan dari Pemesan (', {continued: true}).font(bold).text('"Pemegang Surat Hutang"').font(normal).text(').').font(normal).moveDown(0.5)
  doc.text('Penerbit dengan ini menegaskan bahwa Pemegang Surat Hutang pada tanggal Sertifikat ini merupakan pemegang Surat Hutang yang sah dengan jumlah pokok Rp[  ]. Atas nilai yang diterima tersebut, Penerbit berjanji untuk melakukan pelunasan Kewajiban yang Terhutang sesuai dengan ketentuan yang diatur di dalam Perjanjian.', 70, null, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.text('Sertifikat ini hanya merupakan bukti kepemilikan. Penerbit dengan ini menyatakan dan menegaskan bahwa Sertifikat ini tidak ditawarkan kepada masyarakat dan Penerbit tidak pernah mengajukan permohonan pendaftaran kepada Otoritas Jasa Keuangan sesuai dengan Undang-Undang No. 8 tahun 1995 tentang Pasar Modal dan peraturan pelaksananya. Oleh karena itu, Sertifikat tidak dapat ditawarkan atau dijual dengan cara yang merupakan penawaran umum berdasarkan hukum yang berlaku di Indonesia.', 70, null, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.text('Sertifikat ini diatur berdasarkan, dan harus ditafsirkan sesuai dengan peraturan perundang-undangan yang berlaku di Republik Indonesia.', 70, null, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.font(bold).text('[  ], 2016', 70, 560, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.font(bold).text('Penerbit: ', 70, 580, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.font(bold).text('..............................................................', 70, 660, {align: 'justify'}).font(normal).moveDown(0.5)
  // doc.text('2.2', {continued: true}).text('Para Pihak sepakat atas hal-hal sebagai berikut:', 130, null, {align: 'justify'}).moveDown(0.5)
  doc.pipe(fs.createWriteStream(pnIssuaceFilename))
  doc.end()
  stream.on('finish', function () {
    var result = {message: 'pdf successfully generate', success: true}
    cb(result)
  })
}

function pnIssuanceCel (data, cb) {
  var doc = new Pdf()
  var stream = doc.pipe(blobStream())
  var dir = 'generate_pdf/'
  var bold = 'Times-Bold'
  var italic = 'Times-Italic'
  var bitalic = 'Times-BoldItalic'
  var normal = 'Times-Roman'
  var pnIssuaceFilename = dir + 'PN_ISSUANCE_AGREEMENT(CEL)_' + data.name + '.pdf'
  doc.fontSize(12).font(bold).text('PERJANJIAN PENERBITAN SURAT HUTANG ("Perjanjian") ', 70, 90, {continued: true}).font(normal).text('ini dibuat pada tanggal 2016, antara:', 70, 90).moveDown()
  doc.text('1', 70, 130).text('Jonathan, Warga Negara Indonesia, pemegang Kartu Tanda Penduduk No. , yang berdomisili di ("Penerbit")', 98, 130, {align: 'justify'}).moveDown(0.5)
  doc.text('2', 70, 160).text('PT Indonusa Bara Sejahtera, suatu perseroan terbatas yang didirikan dan tunduk berdasarkan Hukum yang Berlaku di Indonesia, yang berdomisili di DBS Tower ("Agen Fasilitas") dan', 98, 160, {align: 'justify'}).moveDown(0.5)
  doc.text('3', 70, 210).text('PARA PEMESAN yang tercantum dalam Lampiran 1 dari Perjanjian ini, yang dalam hal ini telah memberikan kuasa kepada Agen Fasilitas untuk melakukan pemesanan atas Surat Hutang ("Pemesan"),', 98, 210, {align: 'justify'}).moveDown(0.5)
  doc.text('Penerbit, Agen Fasilitas dan Pemesan dapat disebut secara bersama-sama sebagai "Para Pihak" dan masing-masing sebagai "Pihak".', 70, 260).moveDown()

  doc.font(bold).text('LATAR BELAKANG', 70, 310).font(normal).moveDown(0.5)
  doc.text('(A)', 70, 330).text('Penerbit bermaksud untuk menerbitkan dan menawarkan suatu surat hutang kepada Pemesan dalam jumlah sebesar sebagaimana tercantum di Lampiran 2 Perjanjian ini, yang mana bukti surat hutang tersebut adalah sertifikat surat hutang yang dibuat dengan bentuk sebagaimana tersirat di Lampiran 3 Perjanjian ini ("Surat Hutang") kepada Pemesan dengan tunduk ketentuan Perjanjian ini.', 98, 330, {align: 'justify'}).moveDown(0.5)
  doc.text('(B)', 70, 400).text('Sehubungan dengan penerbitan dan pemesanan Surat Hutang yang dilakukan dari Penerbit kepada Pemesan, Pemesan dengan ini setuju untuk memesan dan membayar Surat Hutang dengan menunjuk Agen Fasilitas untuk dan atas nama Pemesan melakukan segala tindakan yang diperlukan sehubungan dengan memfasilitasi pemesanan, penerbitan, dan hal-hal administrasi lainnya sehubungan dengan Surat Hutang sesuai dengan ketentuan yang diatur di dalam Perjanjian ini.', 98, 400, {align: 'justify'}).moveDown(0.5)
  doc.text('(C)', 70, 490).text('Pembagian hak dan kewajiban diantara Pemesan atas Surat Hutang yang diterbitkan adalah secara proporsional sesuai dengan besaran partisipasi tiap-tiap Pemesan sebagaimana tercantum di Lampiran 1 Perjanjian ini.', 98, 490, {align: 'justify'}).moveDown(0.5)
  doc.font(bold).text('OLEH KARENA ITU, TELAH DISEPAKATI SEBAGAI BERIKUT:', 70, 540).moveDown(0.5)
  doc.text('1.', 70, 560).text('DEFINISI DAN INTERPRETASI', 98, 560, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.text('1.1.', 70, 580).text('Dalam Perjanjian ini, selain dinyatakan lain secara tegas:', 98, 580, {align: 'justify'}).moveDown(0.5)
  doc.font(bold).text('“Angsuran” ', 98, 600, {continued: true, align: 'justify'}).font(normal).text('adalah Kewajiban yang Terhutang, yang harus dibayar secara berkala oleh Penerbit kepada Pemesan melalui Agen Fasilitas sesuai dengan Jadwal Pembayaran Angsuran melalui proses autodebit dari Rekening Gaji.').moveDown(0.5)
  doc.font(bold).text('“Entitas” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti setiap individu, perusahaan/korporasi, perseroan terbatas, kemitraan umum (general partnership), kemitraan terbatas (limited partnership), usaha perseorangan, bentuk organisasi bisnis lainnya, wali amanat (trust), serikat, asosiasi, atau Otoritas Pemerintahan.').moveDown(0.5)
  doc.addPage({margin: {top: 40,
      bottom: 50,
      left: 72,
      right: 72}})
  doc.font(bold).text('“Harga Penerbitan” ', 98, 70, {continued: true, align: 'justify'}).font(normal).text('berarti nominal jumlah hutang yang tercantum di Surat Hutang yang diterbitkan sesuai dengan bentuk sebagaimana tersirat di Lampiran 3 Perjanjian ini.').moveDown(0.5)
  doc.font(bold).text('“Hari Kerja” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti setiap hari.').moveDown(0.5)
  doc.font(bold).text('“Hukum yang Berlaku” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti setiap statuta, undang-undang, regulasi, peraturan (termasuk peraturan pencatatan di bursa efek), Perintah, dan bentuk aturan lainnya yang diakui sebagai hukum yang secara sah berlaku dalam suatu yurisdiksi.').moveDown(0.5)
  doc.font(bold).text('“Jadwal Pembayaran Angsuran” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('jadwal pembayaran Angsuran dari Penerbit yang jatuh pada Tanggal Pembayaran Angsuran, yang rinciannya disebutkan pada Lampiran 2 Perjanjian ini.').moveDown(0.5)
  doc.font(bold).text('“Kerugian” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti setiap bentuk kerusakan dan kerugian yang diderita dan/atau ditanggung secara aktual oleh suatu Entitas.').moveDown(0.5)
  doc.font(bold).text('“Kewajiban” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti segala bentuk kewajiban, klaim, kerugian, kerusakan, komitmen, dan tanggung jawab, baik yang diketahui maupun yang tidak diketahui, yang kontingen ', {continued: true}).font(italic).text('(contigent) ', {continued: true}).font(normal).text('maupun  yang tidak, dari suatu Entitas, berdasarkan atau sehubungan dengan antara lain: perjanjian-perjanjian, Pajak, hutang, dan kewajiban pembayaran yang masih terhutang yang berhubungan dengan kegiatan usaha Entitas tersebut.').moveDown(0.5)
  doc.font(bold).text('“Kewajiban yang Terhutang” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti seluruh Kewajiban yang ada pada saat ini dan di kemudian hari termasuk tetapi tidak terbatas pada suatu jumlah (baik jumlah pokok Surat Hutang, bunga, biaya dan hal-hal lainnya) yang terhutang oleh Penerbit kepada Pemesan.').moveDown(0.5)
  doc.font(bold).text('“Otoritas Pemerintahan” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti pemerintah dari negara apapun atau subdivisi politik lainnya (baik pada tingkat pusat, negara bagian, maupun daerah), yang meliputi: kementerian, agensi, otoritas, badan pengatur, pengadilan, bank sentral atau entitas lainnya yang menjalankan kewenangan di bidang eksekutif, legislatif, yudisial, Perpajakan, moneter, atau administratif, atau menjalankan fungsi yang terkait dengan kegiatan pemerintah sebagaimana diatur dalam Hukum yang Berlaku dari negara yang relevan.').moveDown(0.5)
  doc.font(bold).text('“Pajak” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti segala bentuk perpajakan, pajak kekayaan, pengurangan, pajak potongan ', {continued: true}).font(italic).text('(wittholding)', {continued: true}).font(normal).text(', bea, pembebanan, pungutan, biaya, ongkos, kontribusi jaminan sosial, pendapatan modal, PPN, dan suku bunga yang dikenakan, dipungut, ditagih, dipotong atau dinilai oleh Otoritas Pemerintahan di Indonesia atau dimanapun dan bunga, pajak tambahan, penalti, biaya tambahan atau denda yang terkait dan "Perpajakan" akan diartikan dengan merujuk ke istilah Pajak.').moveDown(0.5)
  doc.font(bold).text('“Penutupan” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti penutupan pelaksanaan Pemesanan Surat Hutang sebagaimana diatur dalam Perjanjian ini.').moveDown(0.5)
  doc.font(bold).text('“Perintah” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti setiap bentuk surat perintah, putusan (termasuk putusan yang melarang atau mewajibkan pihak untuk melakukan suatu tindakan (injunction)), keputusan, atau perintah lainnya yang sejenis dari Otoritas Pemerintahan (baik yang bersifat permulaan maupun final).').moveDown(0.5)
  doc.font(bold).text('“Perkara” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti setiap (i) sengketa, tindakan, gugatan, atau perkara yang dilakukan melalui atau di luar pengadilan, majelis arbitrase, atau alternatif penyelesaian sengketa lainnya, atau (ii) investigasi atau audit yang dilakukan oleh Otoritas Pemerintahan.').moveDown(0.5)

  doc.addPage({margin: {top: 40,
      bottom: 50,
      left: 72,
      right: 72}})
  doc.font(bold).text('“Perusahaan” ', 98, 70, {continued: true, align: 'justify'}).font(normal).text('berarti PT [masukkan nama], suatu perseroan terbatas yang didirikan berdasarkan hukum Indonesia, yang merupakan Entitas yang memperkerjakan, atau memiliki hubungan kerja dengan, Penerbit dan memberikan gaji kepada Penerbit sebagai bentuk kompensasi dari pemberian jasa kerja yang dilakukan oleh Penerbit.').moveDown(0.5)
  doc.font(bold).text('“Rekening Gaji” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti rekening Penerbit yang digunakan sebagai rekening penerimaan gaji Penerbit sehubungan dengan masa kerjanya dengan Perusahaan.').moveDown(0.5)
  doc.font(bold).text('“Rp” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti Rupiah Indonesia, mata uang Republik Indonesia yang sah.').moveDown(0.5)
  doc.font(bold).text('“Sertifikat Surat Hutang” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti sertifikat yang diterbitkan oleh Penerbit kepada Pemesan atas nama Agen Fasilitas (yang dalam kapasitasnya akan memegang Sertifikat Surat Hutang tersebut untuk dan atas kepentingan dari Agen Fasilitas) yang merupakan bagian yang tidak terpisahkan dari Perjanjian ini.').moveDown(0.5)
  doc.font(bold).text('“Surat Pemberitahuan Wanprestasi” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('memiliki arti sebagaimana didefinisikan dalam Pasal 9.2.').moveDown(0.5)
  doc.font(bold).text('“Tanggal Pembayaran Angsuran” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti tanggal dimana suatu Angsuran menjadi jatuh tempo.').moveDown(0.5)
  doc.font(bold).text('“Tanggal Penutupan” ', 98, null, {continued: true, align: 'justify'}).font(normal).text('berarti tanggal yang jatuh pada selambat-lambatnya 2 Hari Kerja setelah terpenuhinya seluruh persyaratan Pemesanan Surat Hutang sebagaimana dimaksud dalam Pasal 4.1 (kecuali ada yang dikesampingkan oleh Pemesan), atau pada tanggal lain sebagaimana disetujui secara tertulis oleh Para Pihak.').moveDown(0.5)
  doc.text('1.2.', 70, 370).text('Setiap rujukan-rujukan, tersurat maupun tersirat, atas Hukum yang Berlaku meliputi seluruh legislasi yang didelegasikan dari Hukum yang Berlaku tersebut dan segala perubahan, konsolidasi, penggantian, atau pengundangan kembali dari Hukum yang Berlaku tersebut.', 98, 370, {align: 'justify'}).moveDown(0.5)
  doc.text('1.3.', 70, 430).text('Referensi dalam perjanjian ini untuk Latar Belakang, Pasal-Pasal dan Lampiran-Lampiran adalah referensi terhadap Latar Belakang, Pasal-Pasal dan Lampiran-Lampiran dari Perjanjian ini (kecuali dinyatakan sebaliknya secara tegas). Lampiran-Lampiran Perjanjian ini merupakan bagian yang tidak terpisahkan dari Perjanjian ini.', 98, 430, {align: 'justify'}).moveDown(0.5)
  doc.text('1.4.', 70, 490).text('Judul-Judul dimasukkan hanya untuk kenyamanan pembacaan dan tidak akan mempengaruhi penafsiran dari Perjanjian ini.', 98, 490, {align: 'justify'}).moveDown(0.5)
  doc.text('1.5.', 70, 530).text('Kapanpun Perjanjian ini merujuk pada suatu tanggal, tanggal tersebut akan merujuk pada hari kalendar Gregorian kecuali dinyatakan sebagai Hari Kerja. Rujukan kepada bulan dan tahun juga merujuk kepada kalender Gregorian.', 98, 530, {align: 'justify'}).moveDown(0.5)
  doc.text('1.6.', 70, 580).text('Istilah "Para Pihak" dan "Pihak", sepanjang konteksnya memungkinkan, meliputi setiap penerusnya yang sah atau pihak penerima pengalihan yang diizinkan berdasarkan Perjanjian ini dan Entitas lainnya yang berhak berdasarkan Perjanjian ini atau Hukum yang Berlaku.', 98, 580, {align: 'justify'}).moveDown(0.5)
  doc.addPage({margin: {top: 40,
      bottom: 50,
      left: 72,
      right: 72}})
  doc.font(bold).text('2.', 70, 70).text('PENERBITAN DAN PEMESANAN SURAT HUTANG', 105, 70, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.text('2.1.', 70, 90).text('Dengan tunduk pada ketentuan dan persyaratan dalam Perjanjian ini, Penerbit dengan ini setuju untuk menerbitkan Surat Hutang, dan Pemesan telah sepakat untuk memesan Surat Hutang tersebut.', 105, 90, {align: 'justify'}).moveDown(0.5)
  doc.text('2.2.', 70, 135).text('Para Pihak sepakat atas hal-hal sebagai berikut:', 105, 135, {align: 'justify'}).moveDown(0.5)
  doc.text('(a)', 105, 160).text('Untuk tujuan pemesanan Surat Hutang yang akan diterbitkan Penerbit, Pemesan (melalui Agen Fasilitas) akan melakukan pembayaran kepada Penerbit sesuai dengan Harga Penerbitan.', 140, 160, {align: 'justify'}).moveDown(0.5)
  doc.text('(b)', 105, 210).text('Bersamaan dengan diterimanya pelunasan pembayaran atas Harga Penerbitan oleh Penerbit, Penerbit wajib menerbitkan Surat Hutang kepada Pemesan dengan cara memberikan Sertifikat Surat Hutang kepada Pemesan. Untuk keperluan penerbitan Sertifikat Surat Hutang tersebut, Pemesan sepakat bahwa Sertifikat Surat Hutang tersebut dapat diterbitkan oleh Penerbit kepada Agen Fasilitas yang akan memegang Sertifikat Surat Hutang Tersebut untuk dan atas kepentingan dari Pemesan.', 140, 210, {align: 'justify'}).moveDown(0.5)
  doc.text('(c)', 105, 310).text('Dana yang diperoleh dari penerbitan Surat Hutang wajib digunakan oleh Penerbit untuk keperluan sebagaimana yang sebelumnya telah disetujui oleh Pemesan.', 140, 310, {align: 'justify'}).moveDown(0.5)
  doc.text('(d)', 105, 340).text('Baik Pemesan maupun Agen Fasilitas tidak terikat untuk memantau atau memeriksa penggunaan jumlah dana yang dipinjam berdasarkan Perjanjian ini.', 140, 340, {align: 'justify'}).moveDown(0.5)
  doc.font(bold).text('3.', 70, 380).text('SURAT HUTANG', 105, 380, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.text('3.1.', 70, 400).text('Surat Hutang yang diterbitkan oleh Penerbit memiliki nilai pokok sebesar sebagaimana tercantum di dalam Surat Hutang tersebut.', 105, 400, {align: 'justify'}).moveDown(0.5)
  doc.text('3.2.', 70, 435).text('Surat Hutang ini diterbitkan untuk berlaku selama masa periode tertentu sebagaimana juga tercantum di dalam Surat Hutang dan sebagai bentuk pelunasan dari Kewajiban yang Terhutang, Penerbit berkewajiban untuk melakukan pembayaran atas Angsuran dan tidak dapat meminjam kembali setiap bagian dari Kewajiban yang Terhutang yang telah dibayarkan kembali kepada Pemesan. Sehubungan dengan hal ini, Para Pihak sepakat bahwa Kewajiban pembayaran atas Angsuran tersebut dilakukan wajib dilakukan oleh Penerbit kepada Agen Pemesan yang dalam kapasitasnya sebagai perwakilan dari Pemesan dalam mengatur pelaksanaan pembayaran tersebut. Pembayaran atas Angsuran akan dilakukan dengan mekanisme autodebet dari Rekening Gaji.', 105, 435, {align: 'justify'}).moveDown(0.5)
  doc.text('3.3.', 70, 565).text('Surat Hutang ini diterbitkan dengan tingkat suku bunga flat tetap sebagaimana tercantum di Surat Hutang tersebut, yang akan dibayarkan secara berkala, dan dihitung dan wajib dibayarkan sebagai bagian dari Angsuran. Dalam hal bunga Surat Hutang ini dianggap sebagai pendapatan bunga oleh Otoritas Pemerintah yang berwenang (termasuk diantaranya Direktorat Jenderal Pajak), maka kewajiban atas pelaporan kewajiban Pajak tahunan Pemesan adalah menjadi tanggung jawab Pemesan itu sendiri.', 105, 565, {align: 'justify'}).moveDown(0.5)
  doc.text('3.4.', 70, 660).text('Para Pihak sepakat bahwa:', 105, 660, {align: 'justify'}).moveDown(0.5)
  doc.addPage({margin: {top: 40,
      bottom: 50,
      left: 72,
      right: 72}})
  // doc.font(normal).text('Nama Pemegang Rekening: PT. Indonusa Bara Sejahtera', 140, 70, {align: 'justify'}).moveDown(0.5)
  // doc.text('Nomor Rekening: 548-5077798 IDR', 140, 90, {align: 'justify'}).moveDown(0.5)
  // doc.text('Nama Bank: BCA KCP Central Park', 140, 110, {align: 'justify'}).moveDown(0.5)
  // doc.font(bold).text('("Rekening Agen Fasilitas")', 140, 130, {align: 'justify'}).font(normal).moveDown(0.5)
  // doc.text('3.3.', 70, 150).text('Surat Hutang ini diterbitkan dengan tingkat suku bunga flat tetap sebagaimana tercantum di Surat Hutang tersebut, yang akan dibayarkan secara berkala, dan dihitung dan wajib dibayarkan sebagai bagian dari Angsuran. Dalam hal bunga Surat Hutang ini dianggap sebagai pendapatan bunga oleh Otoritas Pemerintah yang berwenang (termasuk diantaranya Direktorat Jenderal Pajak), maka kewajiban atas pelaporan kewajiban Pajak tahunan Pemesan adalah menjadi tanggung jawab Pemesan itu sendiri.', 105, 150, {align: 'justify'}).moveDown(0.5)
  // doc.text('3.4.', 70, 255).text('Para Pihak sepakat bahwa:', 105, 255, {align: 'justify'}).moveDown(0.5)
  // doc.text('(a)', 105, 275).text('Dana yang diterima oleh Penerbit dari hasil penerbitan Surat Hutang wajib digunakan oleh Penerbit khusus sebagaimana tercantum di Pasal 2.2 butir c.', 140, 275, {align: 'justify'}).moveDown(0.5)
  // doc.text('(b)', 105, 325).text('Penerbit menyetujui dan mengakui bahwa kegagalan Penerbit untuk mematuhi Kewajibannya dalam Pasal 3.4 (a) tidak akan mempengaruhi keabsahan dari ketentuan Perjanjian ini dalam bentuk apa pun.', 140, 325, {align: 'justify'}).moveDown(0.5)
  // doc.text('3.5.', 70, 375).text('Pada Tanggal Penutupan:', 105, 375, {align: 'justify'}).moveDown(0.5)
  // doc.text('(a)', 105, 395).text('Penerbit wajib menyampaikan kepada Agen Fasilitas dokumen asli dan salinan atas bukti-bukti yang menunjukkan bahwa seluruh persyaratan sebagaimana diatur dalam Pasal 4.1 telah terpenuhi dimana bukti-bukti tersebut harus dalam bentuk yang dapat diterima oleh Agen Fasilitas.', 140, 395, {align: 'justify'}).moveDown(0.5)
  // doc.text('(b)', 105, 455).text('Penerbit wajib melakukan hal-hal sebagai berikut:', 140, 455, {align: 'justify'}).moveDown(0.5)
  // doc.text('(i)', 140, 475).text('menyampaikan informasi kepada Agen Fasilitas mengenai rekening bank Penerbit yang akan menerima pembayaran Harga Penerbitan', 180, 475).font(bold).text('(“Rekening yang Ditunjuk”)', {continued: true}).font(normal).text('; dan', {align: 'justify'}).moveDown(0.5)
  // doc.text('(ii)', 140, 525).text('menyampaikan dokumen-dokumen lainnya sebagaimana diminta secara wajar oleh kepada Agen Fasilitas dan/atau Pemesan untuk memastikan pemenuhan Kewajiban Penerbit berdasarkan ketentuan Perjanjian ini.', 180, 525, {align: 'justify'}).moveDown(0.5)
  // doc.text('(c)', 105, 590).text('Pemesan, melalui Agen Fasilitas, akan menyetorkan komitmen dananya untuk melaksanakan pemesanan Surat Hutang dengan cara melakukan transfer dana senilai Harga Penerbitan ke Rekening yang Ditunjuk setelah ketentuan dalam Pasal 3.5 (a) dan (b) dipenuhi seluruhnya.', 140, 590, {align: 'justify'}).moveDown(0.5)
  // doc.text('(d)', 105, 655).text(' Bersamaan dengan dilaksanakannya penyetoran dana sebagaimana dimaksud dalam Pasal 3.5 (c), Penerbit wajib menyerahkan kepada Agen Fasilitas, Sertifikat Surat Hutang yang membuktikan kepemilikan Pemesan atas Surat Hutang, yang isinya sesuai dengan bentuk yang ditetapkan dalam Lampiran 3.', 140, 655, {align: 'justify'}).moveDown(0.5)
  // doc.addPage({margin: {top: 40,
  //     bottom: 50,
  //     left: 72,
  //     right: 72}})
  // doc.font(bold).text('4.', 70, 70).text('PERSYARATAN-PERSYARATAN', 105, 70, {align: 'justify'}).font(normal).moveDown(0.5)
  // doc.text('4.1.', 70, 90).text('Agar Pemesanan Surat Hutang dapat dilaksanakan, persyaratan-persyaratan di bawah ini wajib dipenuhi terlebih dahulu:', 105, 90, {align: 'justify'}).moveDown(0.5)
  // doc.text('(a)', 105, 125).text('Pemesan telah mendapatkan seluruh Persetujuan yang dibutuhkan untuk menandatangani Perjanjian ini dan melaksanakan seluruh Kewajibannya berdasarkan Perjanjian ini;', 140, 125, {align: 'justify'}).moveDown(0.5)
  // doc.text('(b)', 105, 170).text('seluruh pernyataan dan jaminan berdasarkan Perjanjian ini masih tetap akurat, benar dan tidak menyesatkan baik pada tanggal Perjanjian ini maupun Tanggal Penutupan;', 140, 170, {align: 'justify'}).moveDown(0.5)
  // doc.text('(c)', 105, 215).text('Pemesan telah menandatangani perjanjian dan surat kuasa bermeterai untuk pemotongan rekening (autodebet dari Rekening Uber) dengan Uber, dalam bentuk yang dapat diterima oleh Agen Fasilitas ;', 140, 215, {align: 'justify'}).moveDown(0.5)
  // doc.text('(d)', 105, 260).text('seluruh janji-janji dan Kewajiban yang disyaratkan oleh Pemesan dan/atau Agen Fasilitas untuk dilaksanakan atau patuhi oleh Penerbit berdasarkan Perjanjian ini pada atau sebelum Tanggal Penutupan harus telah dilaksanakan dan dipatuhi seluruhnya;', 140, 260, {align: 'justify'}).moveDown(0.5)
  // doc.text('(e)', 105, 320).text('tidak terjadi keadaan wanprestasi sebagaimana diatur dalam Pasal 9.1; dan', 140, 320, {align: 'justify'}).moveDown(0.5)
  // doc.text('(f)', 105, 340).text('penerbitan dan pemesanan Surat Hutang tidak dilarang berdasarkan Hukum yang Berlaku.', 140, 340, {align: 'justify'}).moveDown(0.5)
  // doc.text('4.2.', 70, 370).text('Jika setiap persyaratan yang diatur dalam Pasal 4.1 tidak terpenuhi sampai dengan jangka waktu 1 minggu sejak tanggal Perjanjian ini atau tanggal lain yang disepakati oleh Pemesan dan Penerbit, Perjanjian ini akan segera berakhir dan dalam keadaan tersebut Para Pihak akan dilepaskan dan dibebaskan dari Kewajiban mereka masing-masing berdasarkan Perjanjian ini dan tidak ada Pihak yang akan memiliki klaim apapun terhadap Pihak lain.', 105, 370, {align: 'justify'}).moveDown(0.5)
  // doc.font(bold).text('5.', 70, 455).text('PEMBAYARAN LEBIH AWAL', 105, 455, {align: 'justify'}).font(normal).moveDown(0.5)
  // doc.text('5.1.', 70, 475).text('Jika, setiap saat berdasarkan Hukum Yang Berlaku, Pemesan menjadi tidak sah untuk melakukan salah satu kewajibannya sebagaimana dimaksud dalam Perjanjian ini, termasuk namun tidak terbatas untuk membeli Surat Hutang dari Penerbit, maka', 105, 475, {align: 'justify'}).moveDown(0.5)
  // doc.text('(a)', 105, 520).text('Pemesan harus segera memberitahukan kepada Agen Fasilitas setelah mengetahui hal tersebut', 140, 520, {align: 'justify'}).moveDown(0.5)
  // doc.text('(b)', 105, 550).text('Agen Fasilitas harus segera memberitahukan hal tersebut kepada Penerbit.', 140, 550, {align: 'justify'}).moveDown(0.5)
  // doc.text('(c)', 105, 570).text('Penerbit harus segera melunasi Kewajiban yang Terhutang segera setelah menerima pemberitahuan dari Agen Fasilitas sebagaimana dimaksud di dalam Pasal 5.1 (ii) di atas.', 140, 570, {align: 'justify'}).moveDown(0.5)
  // doc.text('5.2.', 70, 620).text('Dalam hal Penerbit menghentikan hubungan kemitraannya dengan Uber sebelum seluruh Kewajiban yang Terhutang telah selesai dilunasi oleh Penerbit berdasarkan ketentuan yang diatur di dalam Perjanjian ini, maka seluruh Kewajiban yang Terhutang akan segera menjadi jatuh tempo pada saat berhentinya hubungan kemitraan tersebut dan pada hari yang sama, Penerbit wajib melunasi seluruh Kewajiban Terhutang tersebut kepada Pemesan, melalui Agen Fasilitas.', 105, 620, {align: 'justify'}).moveDown(0.5)
  // doc.addPage({margin: {top: 40,
  //     bottom: 50,
  //     left: 72,
  //     right: 72}})
  // doc.font(normal).text('5.3.', 70, 70).text('Penerbit setiap waktu diperbolehkan untuk membayar kembali seluruh sisa Kewajiban yang Terhutang sebelum berakhir jangka waktunya, dengan ketentuan Penerbit telah menyampaikan surat pemberitahuan tertulis kepada Agen Fasilitas selambat-lambatnya 7 Hari Kerja sebelumnya tentang kehendak untuk melakukan pembayaran kembali yang dipercepat, yang dengan tegas menyebutkan jumlah seluruh sisa Kewajiban yang Terhutang yang akan dilunasi dan kapan akan dilaksanakan pembayaran tersebut, dengan ketentuan pemberitahuan tertulis tersebut tidak dapat ditarik kembali oleh Penerbit.', 105, 70, {align: 'justify'}).moveDown(0.5)
  // doc.font(bold).text('6.', 70, 170).text('DENDA', 105, 170, {align: 'justify'}).font(normal).moveDown(0.5)
  // doc.text('6.1.', 70, 190).text('Dalam hal Penerbit lalai untuk membayar Kewajibannya berdasarkan Perjanjian, baik Angsuran, sisa Kewajiban yang Terhutang, biaya-biaya dan/atau suatu jumlah lain yang terutang dan wajib dibayar oleh Penerbit kepada Pemesan melalui Agen Fasilitas karena sebab apapun pada tanggal jatuh waktunya sebagaimana ditentukan dalam Perjanjian, maka Penerbit wajib dan setuju untuk membayar denda sebesar [0,1% per hari yang dihitung dari Angsuran] untuk setiap hari keterlambatan.', 105, 190, {align: 'justify'}).moveDown(0.5)
  // doc.text('6.2.', 70, 275).text('Pengenaan denda dihitung sejak tanggal jatuh waktu suatu pembayaran sampai dengan tanggal pelunasan Kewajiban tersebut. Denda tersebut harus dibayar secara seketika dan sekaligus lunas bersama dengan pembayaran yang telah lewat waktu dimaksud.', 105, 275, {align: 'justify'}).moveDown(0.5)
  // doc.text('6.3.', 70, 320).text('Perhitungan denda tersebut dalam Pasal 6.1 Perjanjian ini dilakukan secara harian atas dasar pembagi tetap 30 hari dalam sebulan.', 105, 320, {align: 'justify'}).moveDown(0.5)
  // doc.font(bold).text('7.', 70, 355).text('PERNYATAAN', 105, 355, {align: 'justify'}).font(normal).moveDown(0.5)
  // doc.text('7.1.', 70, 375).text('Penerbit menyatakan kepada Pemesan bahwa terhitung sejak tanggal Perjanjian ini, pernyataan dan informasi sebagaimana dinyatakan di bawah ini adalah benar, tepat dan tidak menyesatkan secara material ', 105, 375, {align: 'justify', continued: true}).font(bold).text('("Pernyataan Penerbit"):').font(normal).moveDown(0.5)
  // doc.text('(a)', 105, 420).text('Penerbit memiliki kekuasaan untuk memiliki asetnya dan memiliki kekuasaan penuh, wewenang dan  hak hukum untuk menerbitkan Surat Hutang dan menandatangani Perjanjian ini.', 140, 420, {align: 'justify'}).moveDown(0.5)
  // doc.text('(b)', 105, 465).text('Penerbit tidak dalam proses kepailitan, penundaan Kewajiban pembayaran hutang atau proses-proses semacamnya dan tidak ada permohonan untuk kepailitan, penundaan Kewajiban pembayaran hutang atau proses semacamnya yang sedang dilakukan.', 140, 465, {align: 'justify'}).moveDown(0.5)
  // doc.text('(c)', 105, 520).text('Perjanjian ini dan Surat Hutang dianggap sah, berlaku dan mengikat Kewajiban Penerbit dapat dilaksanakan sesuai dengan ketentuan-ketentuannya.', 140, 520, {align: 'justify'}).moveDown(0.5)
  // doc.text('(d)', 105, 550).text('Penandatanganan Perjanjian ini dan penerbitan Surat Hutang untuk kepentingan Penerbit.', 140, 550, {align: 'justify'}).moveDown(0.5)
  // doc.text('(e)', 105, 580).text('seluruh informasi yang diberikan dan dokumen-dokumen yang dikirimkan kepada Pemesan dan Agen Fasilitas oleh atau atas nama Penerbit adalah benar dan akurat dalam semua hal.', 140, 580, {align: 'justify'}).moveDown(0.5)
  // doc.addPage({margin: {top: 40,
  //     bottom: 50,
  //     left: 72,
  //     right: 72}})
  // doc.font(normal).text('(f)', 105, 70).text('Penerbit umumnya tunduk pada hukum perdata, hukum usaha dan atas Perkara hukum, dan baik Penerbit maupun asetnya atau pendapatannya tidak berhak atas hak kekebalan atau hak istimewa (berdaulat atau sebaliknya) dari setiap penjumpaan utang (set-off), pemutusan, eksekusi, sita jaminan atau proses hukum lainnya.', 140, 70, {align: 'justify'}).moveDown(0.5)
  // doc.text('7.2.', 70, 145).text('Penerbit mengakui bahwa Pemesan mengikat Perjanjian ini dengan mengandalkan kebenaran dan keakuratan Pernyataan Penerbit.', 105, 145, {align: 'justify'}).moveDown(0.5)
  // doc.font(bold).text('8.', 70, 180).text('JANJI-JANJI', 105, 180, {align: 'justify'}).font(normal).moveDown(0.5)
  // doc.text('8.1.', 70, 200).text('Selain dari Kewajiban lainnya yang diatur dalam Perjanjian ini, sejak tanggal Perjanjian ini sampai dengan pelunasan penuh Kewajiban yang Terhutang, Penerbit sepakat untuk mematuhi dan melaksanakan seluruh Kewajibannya sebagaimana dinyatakan di bawah ini.', 105, 200, {align: 'justify'}).moveDown(0.5)
  // doc.text('(a)', 105, 250).text('Penerbit tidak akan mengajukan, dan tidak akan memberikan persetujuan kepada suami atau istrinya untuk mengajukan, permohonan kepailitan atau penundaan Kewajiban pembayaran hutang atau prosedur serupa lainnya atas dirinya sendiri atau atas  suami atau istrinya.', 140, 250, {align: 'justify'}).moveDown(0.5)
  // doc.text('(b)', 105, 310).text('Penerbit tidak akan menjual atau dengan cara lain mengalihkan, membagikan atau melepaskan semua atau sebagian usahanya atau asset atau pendapatannya, baik dalam satu atau beberapa transaksi yang terkait maupun tidak tidak terkait, yang mungkin mempengaruhi kemampuan Penerbit untuk melakukan pelunasan atas Kewajiban yang Terhutang.', 140, 310, {align: 'justify'}).moveDown(0.5)
  // doc.text('(c)', 105, 385).text('Penerbit tidak akan mengadakan perjanjian atau kewajiban lainnya yang mungkin menyebabkan Penerbit tidak mampu untuk melakukan pelunasan atas Kewajiban yang Terhutang.', 140, 385, {align: 'justify'}).moveDown(0.5)
  // doc.text('(d)', 105, 430).text('Penerbit harus memastikan Kewajibannya berdasarkan Perjanjian ini setiap saat paling tidak pari passu dengan utang Penerbit lainnya.', 140, 430, {align: 'justify'}).moveDown(0.5)
  // doc.text('(e)', 105, 465).text('Penerbit harus tepat waktu membayar semua Kewajiban yang Terhutang yang harus dilunasi sesuai dengan ketentuan yang diatur di dalan Perjanjian ini.', 140, 465, {align: 'justify'}).moveDown(0.5)
  // doc.font(bold).text('9.', 70, 500).text('KEADAAN WANPRESTASI', 105, 500, {align: 'justify'}).font(normal).moveDown(0.5)
  // doc.text('9.1.', 70, 520).text('Salah satu atau lebih dari kejadian-kejadian atau hal-hal-tersebut di bawah ini akan dianggap sebagai wanprestasi dari Penerbit apabila terjadi sebelum dilunasinya Kewajiban yang Terhutang secara keseluruhan. ', 105, 520, {align: 'justify'}).moveDown(0.5)
  // doc.text('(a)', 105, 570).text('Penerbit tidak melakukan pembayaran atas seluruh maupun sebagian Kewajiban yang Terhutang yang telah jatuh tempo dan harus dibayar atau terhutang pada saat jatuh tempo sesuai dengan ketentuan-ketentuan dalam Perjanjian ini kecuali kelalaian tersebut disebabkan hal administratif dan teknis, dan pembayaran akhirnya terlaksana dalam waktu 2 Hari Kerja setelah tanggal jatuh tempo yang relevan;', 140, 570, {align: 'justify'}).moveDown(0.5)
  // doc.text('(b)', 105, 660).text('Setiap pernyataan atau jaminan yang dibuat oleh Penerbit berdasarkan Pasal 7 ternyata tidak benar, secara material, tidak sesuai dengan kenyataan sebenarnya atau menyesatkan, atau terjadi pelanggaran atas janji-janji berdasarkan Pasal 8 (baik janji untuk melakukan atau janji untuk tidak melakukan) sebagaimana disyaratkan dalam Perjanjian ini dan jika keadaan tersebut dapat diperbaiki, hal itu tidak diperbaiki dalam waktu 5 Hari Kerja setelah Agen Fasilitas menyampaikan pemberitahuan kepada Penerbit; atau', 140, 660, {align: 'justify'}).moveDown(0.5)
  // doc.text('(c)', 105, 120).text('Penerbit menyatakan secara tertulis dan secara umum tidak dapat membayar utangnya pada tanggal jatuh tempo atau mengajukan Penundaan Kewajiban Pembayaran Utang (PKPU) ke pengadilan niaga, berhenti atau menghentikan sementara pembayaran-pembayaran kepada kreditur-kreditur atau tidak dapat atau mengakui ketidak sanggupannya untuk membayar hutang-hutangnya pada waktu jatuh tempo atau dinyatakan jatuh pailit.', 140, 120, {align: 'justify'}).moveDown(0.5)
  // doc.text('9.2.', 70, 210).text('Jika suatu Wanprestasi telah terjadi maka Agen Fasilitas dapat memberikan surat pemberitahuan Wanprestasi ', 105, 210, {align: 'justify', continued: true}).font(bold).text('(“Surat Pemberitahuan Wanprestasi”) ', {continued: true}).font(normal).text('dan setelahnya (kecuali Penerbit dapat memperbaiki Wanprestasi tersebut sesuai dengan jangka waktu yang diatur di dalam Perjanjian ini) maka Agen Fasilitas berhak memberitahukan secara tertulis kepada Penerbit:').moveDown(0.5)
  // doc.text('(a)', 105, 280).text('menyatakan jumlah Kewajiban yang Terhutang yang seketika itu jatuh tempo dan harus dibayar oleh Penerbit tanpa pemberitahuan lebih lanjut; dan/atau', 140, 280, {align: 'justify'}).moveDown(0.5)
  // doc.text('(b)', 105, 310).text('menyatakan Perjanjian ini berakhir tanpa mengurangi hak Pemesan dan Agen Fasilitas untuk menuntut hak-haknya berdasarkan Perjanjian ini.', 140, 310, {align: 'justify'}).moveDown(0.5)
  // doc.font(bold).text('10.', 70, 350).text('PENGAKHIRAN', 105, 350, {align: 'justify'}).font(normal).moveDown(0.5)
  // doc.text('10.1.', 70, 370).text('Perjanjian ini dapat diakhiri berdasarkan hal-hal sebagai berikut:', 105, 370, {align: 'justify'}).moveDown(0.5)
  // doc.text('(a)', 105, 390).text('berdasarkan persetujuan tertulis oleh Para Pihak;', 140, 390, {align: 'justify'}).moveDown(0.5)
  // doc.text('(b)', 105, 410).text('terjadinya keadaan Wanprestasi sebagaimana diatur dalam Pasal 9.1; atau', 140, 410, {align: 'justify'}).moveDown(0.5)
  // doc.text('(c)', 105, 430).text('dilunasinya seluruh Kewajiban yang Terhutang oleh Penerbit kepada Pemesan, melalui Agen Fasilitas.', 140, 430, {align: 'justify'}).moveDown(0.5)
  // doc.text('10.2.', 70, 470).text('Kecuali dinyatakan sebaliknya dalam Pasal 10.3 dan Pasal 10.4, seluruh hak dan Kewajiban Para Pihak akan berakhir dengan segera setelah pengakhiran Perjanjian ini dan tidak ada Pihak yang harus bertanggung jawab terhadap Pihak lainnya atas Kewajiban-Kewajiban apapun sebagai akibat dari pengakhiran dari Perjanjian.', 105, 470, {align: 'justify'}).moveDown(0.5)
  // doc.text('10.3.', 70, 530).text('Pengakhiran Perjanjian ini tidak akan berdampak terhadap hak masing-masing Pihak untuk menerima ganti rugi dari Pihak lainnya sehubungan dengan ketentuan dalam Perjanjian ini sepanjang hak tersebut sudah timbul sebelum atau pada saat Perjanjian ini diakhiri.', 105, 530, {align: 'justify'}).moveDown(0.5)
  // doc.text('10.4.', 70, 590).text('Ketentuan dalam Pasal 9.2, 10.2, 10.3, 10.4, 11, 12.10 dan 12.11 akan tetap berlaku setelah Perjanjian ini diakhiri.', 105, 590, {align: 'justify'}).moveDown(0.5)
  // doc.text('10.5.', 70, 625).text('Para Pihak dengan tidak dapat ditarik kembali setuju untuk mengesampingkan ketentuan Pasal 1266 dari Kitab Undang-Undang Hukum Perdata sepanjang penetapan pengadilan diperlukan untuk pembatalan atau pengakhiran terlebih dahulu dari Perjanjian ini.', 105, 625, {align: 'justify'}).moveDown(0.5)
  // doc.addPage({margin: {top: 40,
  //     bottom: 50,
  //     left: 72,
  //     right: 72}})
  // doc.font(bold).text('11.', 70, 70).text('GANTI RUGI', 105, 70, {align: 'justify'}).font(normal).moveDown(0.5)
  // doc.text('11.1.', 70, 90).text('Penerbit wajib memberikan ganti rugi kepada Pemesan dan/atau Agen Fasilitas sehubungan dengan: (i) Kerugian yang secara nyata dialami oleh Pemesan dan/atau Agen Fasilitas sebagai akibat kegagalan Penerbit dalam melaksanakan kewajibannya berdasarkan Perjanjian ini (termasuk segala bentuk wanprestasi sebagaimana diatur dalam Pasal 9.1), dan (ii) seluruh biaya sehubungan dengan dokumentasi (termasuk biaya konsultan hukum yang wajar) dan biaya lainnya yang dikeluarkan oleh Pemesan untuk menuntut hak Pemesan dan/atau Agen Fasilitas atas Kerugian sebagaimana disebutkan di atas.', 105, 90, {align: 'justify'}).moveDown(0.5)
  // doc.text('11.2.', 70, 205).text('Semua klaim penggantian Kerugian berdasarkan Pasal 11.1 ini harus dibayarkan oleh Penerbit kepada Pemesan dan/atau Agen Fasilitas dalam jangka waktu 5 Hari Kerja setelah pemberitahuan tertulis terhadap klaim-klaim tersebut berikut dengan bukti-bukti pendukung telah disampaikan oleh Pemesan dan/atau Agen Fasilitas kepada Penerbit.', 105, 205, {align: 'justify'}).moveDown(0.5)
  // doc.font(bold).text('12.', 70, 270).text('LAIN-LAIN', 105, 270, {align: 'justify'}).font(normal).moveDown(0.5)
  // doc.text('12.1.', 70, 290).text('Semua pemberitahuan, permintaan dan bentuk komunikasi lainnya harus dilakukan secara tertulis dalam Bahasa Indonesia dan akan dianggap telah diberikan apabila pemberitahuan tersebut diserahkan secara personal atau dengan transmisi faksimili atau melalui pos (pos prabayar kelas pertama) atau dikirimkan melalui surat elektronik kepada Para Pihak dengan alamat di bawah ini:', 105, 290, {align: 'justify'}).moveDown(0.5)
  // doc.text('Apabila kepada Pemesan dan/atau Agen Fasilitas:').moveDown(0.5)
  // doc.font(bold).text('PT Indonusa Bara Sejahtera').font(normal).moveDown(0.5)
  // doc.text('Alamat', 105, 400).text(':', 170, 400).text('Rukan Golden 8 No.8C Jalan Panjang, Kedoya Utara Jakarta Barat, 11520 Indonesia.', 200, 400).moveDown(0.5)
  // doc.text('Telp. No.', 105, 430).text(':', 170, 430).text('021 - 2920095', 200, 430).moveDown(0.5)
  // doc.text('Fax. No.', 105, 450).text(':', 170, 450).text('-', 200, 450).moveDown(0.5)
  // doc.text('Email', 105, 470).text(':', 170, 470).text('admin@taralite.com', 200, 470).moveDown(0.5)
  // doc.text('U.P.', 105, 490).text(':', 170, 490).text('Abraham Viktor', 200, 490).moveDown(0.5)
  // doc.text('Apabila kepada Penerbit:', 105, 510).moveDown(0.5)
  // doc.text('Alamat', 105, 530).text(':', 170, 530).moveDown(0.5)
  // doc.text('No. Telepon', 105, 550).text(':', 170, 550).moveDown(0.5)
  // doc.text('Setiap Pihak dari waktu ke waktu dapat mengganti alamat, nomor faksimili atau informasi lainnya dengan tujuan untuk melakukan pemberitahuan-pemberitahuan kepada Pihak lainnya dengan memberikan pemberitahuan terkait perubahan informasi tersebut kepada Pihak lainnya.', 105, 570, {align: 'justify'})
  // doc.text('12.2', 70, 640).text('Ketentuan dan kondisi Perjanjian ini dapat dikesampingkan dari waktu ke waktu oleh Para Pihak yang berhak atas pemberian pengesampingan tersebut, tetapi tidak ada pengesampingan yang akan menjadi efektif kecuali pengesampingan tersebut dituangkan dalam instrumen tertulis yang ditandatangani oleh atau atas nama Pihak yang mengesampingkan ketentuan atau kondisi tersebut.', 105, 640, {align: 'justify'}).moveDown(0.5)
  // doc.addPage({margin: {top: 40,
  //     bottom: 50,
  //     left: 72,
  //     right: 72}})
  // doc.font(normal).text('12.3', 70).text('Perjanjian ini hanya dapat diubah, ditambah, atau dimodifikasi dengan instrumen tertulis yang ditandatangani secara bersama-sama oleh atau atas nama Para Pihak.', 105, 70, {align: 'justify'}).moveDown(0.5)
  // doc.text('12.4', 70, 105).text('Tanpa persetujuan tertulis terlebih dahulu dari Penerbit dan cukup dengan diberikannya suatu pemberitahuan secara tertulis dari Agen Fasilitas kepada Penerbit, Pemesan berhak untuk mengalihkan kepada pihak ketiga sebagian atau seluruh hak dan Kewajibannya berdasarkan Perjanjian ini dengan cara apapun dan Penerbit dengan ini memberikan persetujuannya atas hal tersebut. Penerbit berjanji dan mengikat diri untuk atas permintaan pertama Pemesan, menandatangani, membuat dan menyerahkan dokumen-dokumen yang diminta Pemsan dan melaksanakan perjanjian yang dibuat sehubungan dengan pengalihan dimaksud. Perjanjian tetap mengikat para pengganti hak masing-masing pihak. Selanjutnya Penerbit tidak berhak untuk mengalihkan atau memindahkan dengan cara apapun sebagian atau seluruh hak-hak dan Kewajiban-Kewajibannya berdasarkan Perjanjian tanpa persetujuan tertulis terlebih dahulu dari Pemesan.', 105, 105, {align: 'justify'}).moveDown(0.5)
  // doc.text('12.5', 70, 260).text('Apabila ada ketentuan dalam Perjanjian ini yang dinyatakan ilegal, tidak sah, atau tidak dapat dilaksanakan berdasarkan Hukum yang Berlaku saat ini atau masa mendatang, dan apabila hak atau Kewajiban Para Pihak berdasarkan Perjanjian ini tidak akan merugikan secara material:', 105, 260, {align: 'justify'}).moveDown(0.5)
  // doc.text('(a)', 105, 320).text('ketentuan tersebut akan dipisahkan secara sepenuhnya;', 140, 320, {align: 'justify'}).moveDown(0.5)
  // doc.text('(b)', 105, 340).text('Perjanjian ini akan ditafsirkan dan berlaku seakan-akan ketentuan yang ilegal, tidak sah, atau tidak dapat dilaksanakan tersebut tidak pernah menjadi bagian dari Perjanjian ini; dan', 140, 340, {align: 'justify'}).moveDown(0.5)
  // doc.text('(c)', 105, 390).text('ketentuan lainnya dari Perjanjian ini akan tetap berlaku penuh dan tidak akan terpengaruh oleh ketentuan yang ilegal, tidak sah, atau tidak dapat dilaksanakan atau oleh pemisahan dari ketentuan yang bersangkutan.', 140, 390, {align: 'justify'}).moveDown(0.5)
  // doc.text('12.6', 70, 440).text('Kecuali diwajibkan berdasarkan peraturan perundang-undangan yang berlaku, Para Pihak setuju untuk tidak mengungkapkan isi Perjanjian kepada pihak ketiga manapun tanpa memperoleh persetujuan terlebih dahulu dari Pihak lainnya.', 105, 440, {align: 'justify'}).moveDown(0.5)
  // doc.text('12.7', 70, 490).text('Perjanjian ini dapat ditandatangani dalam beberapa salinan dimana tiap-tiap salinan tersebut dianggap asli dan memiliki keberlakuan yang sama dan salinan-salinan tersebut akan dianggap sebagai satu kesatuan dokumen yang asli dan akan mengikat Para Pihak seakan-akan Perjanjian ini hanya terdiri atas satu dokumen.', 105, 490, {align: 'justify'}).moveDown(0.5)
  // doc.text('12.8', 70, 550).text('Perjanjian ini beserta hak-hak dan Kewajiban-Kewajiban yang timbul atau berkenaan dengannya tunduk pada hukum Negara Republik Indonesia.', 105, 550, {align: 'justify'}).moveDown(0.5)
  // doc.text('12.9', 70, 580).text('Para pihak dengan ini sepakat bahwa Pengadilan Negeri Jakarta Barat mempunyai yurisdiksi yang non-eksklusif untuk memeriksa dan memutuskan segala gugatan, tuntutan atau tindakan hukum yang timbul berkenaan dengan Perjanjian.', 105, 580, {align: 'justify'}).moveDown(0.5)
  // doc.text('12.10', 70, 630).text('Jika setiap Sertifikat Surat Hutang terpotong, rusak, hancur, dicuri atau hilang, sertifikat tersebut dapat diganti oleh Penerbit setelah Agen Fasilitas melakukan pembayaran kepada Penerbit atas biaya yang mungkin dikenakan sehubungan dengan penggantian tersebut, sebagaimana Penerbit dapat minta. Sertifikat Surat Hutang yang rusak atau hancur harus diserahkan kepada Penerbit sebelum penggantinya diterbitkan.', 105, 630, {align: 'justify'}).moveDown(0.5)
  // doc.addPage({margin: {top: 40,
  //     bottom: 50,
  //     left: 72,
  //     right: 72}})
  // doc.font(normal).text('12.11', 70, 70).text('Para Pihak sepakat bahwa pembayaran yang dilakukan oleh Penerbit secara tunai (baik dalam konteks melakukan pembayaran terhadap Kewajiban yang Terhutang, Angsuran, ganti Kerugian, dan lainnya) wajib ditujukan kepada Rekening Agen Fasilitas.', 105, 70, {align: 'justify'}).moveDown(0.5)
  // doc.font(italic).text('[HALAMAN TANDA TANGAN ADA PADA HALAMAN SELANJUTNYA]', 70, null, {align: 'center'}).font(normal).moveDown(0.5)
  // doc.addPage({margin: {top: 40,
  //     bottom: 50,
  //     left: 72,
  //     right: 72}})
  // doc.font(italic).text('DITANDATANGANI pada tanggal yang pertama kali dituliskan di atas', 70, 70).font(normal).moveDown(0.5)
  // doc.font(bold).text('PENERBIT', 70, 90).font(normal).moveDown(0.5)
  // doc.font(italic).text('Oleh', 70, 170, {continued: true}).text('                               ', 105, 170, {underline: true}).font(normal).moveDown(0.5)
  // doc.font(italic).text('Nama', 70, 190, {continued: true}).font(normal).text(':', 99, 190).moveDown(0.5)
  // doc.font(bold).text('PEMESAN', 70, 240).font(normal).moveDown(0.5)
  // doc.font(bold).text('dalam hal ini diwakili oleh', 70, 260).font(normal).moveDown(0.5)
  // doc.font(bold).text('PT INDONUSA BARA SEJAHTERA', 70, 280).font(normal).moveDown(0.5)
  // doc.font(italic).text('Oleh', 70, 360, {continued: true}).text('                               ', 105, 360, {underline: true}).font(normal).moveDown(0.5)
  // doc.font(italic).text('Nama', 70, 380, {continued: true}).font(normal).text(':', 99, 380, {continued: true}).text('Abraham Viktor', 110, 380).moveDown(0.5)
  // doc.font(italic).text('Jabatan', 70, 400, {continued: true}).font(normal).text(':', 90, 400, {continued: true}).text('Direktur Utama', 100, 400).moveDown(0.5)
  // doc.font(bold).text('AGEN FASILITAS', 70, 450).font(normal).moveDown(0.5)
  // doc.font(bold).text('PT INDONUSA BARA SEJAHTERA', 70, 470).font(normal).moveDown(0.5)
  // doc.font(italic).text('Oleh', 70, 560, {continued: true}).text('                               ', 105, 560, {underline: true}).font(normal).moveDown(0.5)
  // doc.font(italic).text('Nama', 70, 580, {continued: true}).font(normal).text(':', 99, 580).moveDown(0.5)
  // doc.font(italic).text('Jabatan', 70, 600, {continued: true}).font(normal).text(':', 90, 600).moveDown(0.5)
  // doc.addPage({margin: {top: 40,
  //     bottom: 50,
  //     left: 72,
  //     right: 72}})
  // doc.font(bold).text('LAMPIRAN 1', 70, 70, {align: 'center'}).font(normal).moveDown(0.5)
  // doc.font(bold).text('DAFTAR PEMESAN', 70, 90, {align: 'center'}).font(normal).moveDown(0.5)
  // doc.text('No', 70, 140).text('Nama', 98, 140).text('Partisipasi', 395, 140).moveDown(0.5)
  // doc.text('1', 70, 160).text('Abraham Viktor', 98, 160).text('Rp.', 395, 160).moveDown(3)
  // doc.font(bitalic).text('[Note: Format untuk disesuaikan oleh Taralite]', 70, null, {align: 'left'})
  //
  // doc.addPage({margin: {top: 40,
  //     bottom: 50,
  //     left: 72,
  //     right: 72}})
  // doc.font(bold).text('LAMPIRAN 2', 70, 70, {align: 'center'}).font(normal).moveDown(0.5)
  // doc.font(bold).text('JUMLAH POKOK HUTANG SERTA JADWAL PEMBAYARAN ANGSURAN', 70, 90, {align: 'center'}).font(normal).moveDown(0.5)
  // var table = new PdfTable(doc, {
  //   bottomMargin: 30
  // })
  // table
  // .addPlugin(new (require('voilab-pdf-table/plugins/fitcolumn'))({
  //   column: 'num'
  // }))
  // .setColumnsDefaults({
  //   headerBorder: 'B',
  //   align: 'right'
  // })
  // .addColumns([
  //   {
  //     id: 'num',
  //     header: 'Angsuran',
  //     align: 'left',
  //     width: 80
  //   },
  //   {
  //     id: 'amount',
  //     header: 'Jumlah Pembayaran Kewajiban yang Terhutang (termasuk Bunga)',
  //     align: 'center',
  //     width: 240
  //   },
  //   {
  //     id: 'date',
  //     header: 'Tanggal  Pembayaran Angsuran',
  //     align: 'center',
  //     width: 150
  //   }
  // ])
  // .onPageAdded(function (tb) {
  //   tb.addHeader()
  // })
  // table.addBody([
  //   {num: '1', amount: '', date: ''},
  //   {num: '2', amount: '', date: ''},
  //   {num: '3', amount: '', date: ''}
  // ])
  // doc.addPage({margin: {top: 40,
  //     bottom: 50,
  //     left: 72,
  //     right: 72}})
  // doc.font(bold).text('LAMPIRAN 3', 70, 70, {align: 'center'}).font(normal).moveDown(0.5)
  // doc.font(bold).text('BENTUK SERTIFIKAT SURAT HUTANG', 70, 90, {align: 'center'}).font(normal).moveDown(0.5)
  // doc.font(bold).text('SERTIFIKAT SURAT HUTANG', 70, 130, {align: 'center'}).font(normal).moveDown(0.5)
  // doc.font(bold).text('Jumlah Pokok', 70, 150).text(':', 170, 150).text('Rp [  ]', 190, 150).font(normal).moveDown(0.5)
  // doc.font(bold).text('Tenor', 70, 170).text(':', 170, 170).text('[  ]  minggu', 190, 170).font(normal).moveDown(0.5)
  // doc.font(bold).text('Jenis', 70, 190).text(':', 170, 190).text('angsuran tetap mingguan', 190, 190).font(normal).moveDown(0.5)
  // doc.font(bold).text('Bunga (flat)', 70, 210).text(':', 170, 210).text('[  ] % per minggu', 190, 210).font(normal).moveDown(0.5)
  // doc.text('No.', 70, 230).text(':', 170, 230).text('[  ]', 190, 230).font(normal).moveDown(0.5)
  // doc.text('Tanggal Penerbitan', 70, 250).text(':', 170, 250).text('[  ] 2016', 190, 250).font(normal).moveDown(0.5)
  // doc.text('Sertifikat Surat Hutang (', 70, 270, {continued: true}).font(bold).text('"Sertifikat"', {continued: true}).font(normal).text(') ini diterbitkan oleh [  ] (', {continued: true}).font(bold).text('"Penerbit"', {continued: true}).font(normal).text(') dengan syarat-syarat dan ketentuan yang diatur di dalam, dan merupakan bagian yang tidak terpisahkan dari, Perjanjian Penerbitan Surat Hutang tanggal [  ] 2016 (', {continued: true, align: 'justify'}).font(bold).text('"Perjanjian"', {continued: true}).font(normal).text(') antara Penerbit dengan PEMESAN yang terdaftar dalam Lampiran 1 dari Perjanjian (', {continued: true}).font(bold).text('"Pemesan"', {continued: true}).font(normal).text(') dan PT Indonusa Bara Sejahtera selaku Agen Fasilitas yang dalam hal ini telah diberikan kuasa dari Pemesan untuk memegang Surat Hutang untuk dan atas kepentingan dari Pemesan (', {continued: true}).font(bold).text('"Pemegang Surat Hutang"').font(normal).text(').').font(normal).moveDown(0.5)
  // doc.text('Penerbit dengan ini menegaskan bahwa Pemegang Surat Hutang pada tanggal Sertifikat ini merupakan pemegang Surat Hutang yang sah dengan jumlah pokok Rp[  ]. Atas nilai yang diterima tersebut, Penerbit berjanji untuk melakukan pelunasan Kewajiban yang Terhutang sesuai dengan ketentuan yang diatur di dalam Perjanjian.', 70, null, {align: 'justify'}).font(normal).moveDown(0.5)
  // doc.text('Sertifikat ini hanya merupakan bukti kepemilikan. Penerbit dengan ini menyatakan dan menegaskan bahwa Sertifikat ini tidak ditawarkan kepada masyarakat dan Penerbit tidak pernah mengajukan permohonan pendaftaran kepada Otoritas Jasa Keuangan sesuai dengan Undang-Undang No. 8 tahun 1995 tentang Pasar Modal dan peraturan pelaksananya. Oleh karena itu, Sertifikat tidak dapat ditawarkan atau dijual dengan cara yang merupakan penawaran umum berdasarkan hukum yang berlaku di Indonesia.', 70, null, {align: 'justify'}).font(normal).moveDown(0.5)
  // doc.text('Sertifikat ini diatur berdasarkan, dan harus ditafsirkan sesuai dengan peraturan perundang-undangan yang berlaku di Republik Indonesia.', 70, null, {align: 'justify'}).font(normal).moveDown(0.5)
  // doc.font(bold).text('[  ], 2016', 70, 560, {align: 'justify'}).font(normal).moveDown(0.5)
  // doc.font(bold).text('Penerbit: ', 70, 580, {align: 'justify'}).font(normal).moveDown(0.5)
  // doc.font(bold).text('..............................................................', 70, 660, {align: 'justify'}).font(normal).moveDown(0.5)
  doc.pipe(fs.createWriteStream(pnIssuaceFilename))
  doc.end()
  stream.on('finish', function () {
    var result = {message: 'pdf successfully generate', success: true}
    cb(result)
  })
}

function PdfController () {}
PdfController.prototype = (function () {
  return {
    generate: function generate (request, reply) {
      var type = request.payload.type
      var data = {}
      data.name = request.payload.name
      data.amount = request.payload.amount
      data.term = request.payload.term
      data.payment = request.payload.payment
      data.interest = request.payload.interest
      data.numContract = request.payload.number
      data.releaseDate = request.payload.date
      if (type === 'tokopedia') {
        suratHutang(data, function (hutang) {
          if (hutang.success) {
            pnIssuanceTokopedia(data, function (pnIssuance) {
              reply({message: pnIssuance.message})
            })
          }
        })
      } else if (type === 'uber') {
        suratHutang(data, function (hutang) {
          if (hutang.success) {
            pnIssuanceUber(data, function (pnIssuance) {
              reply({message: pnIssuance.message})
            })
          }
        })
      } else if (type === 'cel') {
        suratHutang(data, function (hutang) {
          if (hutang.success) {
            pnIssuanceCel(data, function (pnIssuance) {
              reply({message: pnIssuance.message})
            })
          }
        })
      } else {
        reply({message: 'loan type is not defined'})
      }
    }
  }
})()

var pdfController = new PdfController()
module.exports = pdfController
