'use strict'

var multiparty = require('multiparty')
var fs = require('fs')
var Path = require('path')
var Converter = require('csvtojson').Converter
var Promise = require('bluebird')
var Bookshelf = require('../config/bookshelf')
var Forms = require('../models/forms')
var Boom = require('boom')

function UploadController () {}
UploadController.prototype = (function () {
  return {
    upload: function upload (request, reply) {
      var form = new multiparty.Form()
      form.parse(request.payload, function (err, fields, files) {
        if (err) return reply(err)
        else {
          fs.readFile(files.files[0].path, function (err, data) {
            var newpath = Path.join(__dirname, '../uploads/' + files.files[0].originalFilename)
            fs.writeFile(newpath, data, function (err) {
              if (err) return reply(err)
              else {
                var i
                var CsvConverter = new Converter({})
                CsvConverter.on('end_parsed', function (jsonArray) {
                  if (jsonArray) {
                    var inputan = []
                    for (i = 0; i < jsonArray.length; i++) {
                      // var jumlahPinjaman = jsonArray[i]['Jumlah Pinjaman'].substring(3).replace(/\./g,'')
                      // jumlahPinjaman = parseInt(jumlahPinjaman)
                      // var ktp = jsonArray[i]['KTP'].toString()
                      var approvalAmount
                      if (jsonArray[i]['Approval Amount'] !== '-') {
                        approvalAmount = jsonArray[i]['Approval Amount'].substring(3).replace(/\./g, '')
                        approvalAmount = parseInt(approvalAmount)
                      } else {
                        approvalAmount = 0
                      }
                      // var noTelepon = '0' + jsonArray[i]['No Telepon']
                      // var kodePos = jsonArray[i]['Kode Pos'].toString()
                      // var rt = jsonArray[i]['RT'].toString()
                      // var rw = jsonArray[i]['RW'].toString()
                      // var kodeJenisUsaha = jsonArray[i]['Kode Jenis Usaha'].toString()
                      inputan.push({
                        nama: jsonArray[i]['Nama'],
                        email: jsonArray[i]['Email'],
                        tipePinjaman: jsonArray[i]['Borrowers Type (Uber/Toped, etc)'],
                        tujuanPinjaman: jsonArray[i]['Tujuan Pinjaman'],
                        alamat: jsonArray[i]['Alamat'],
                        kecamatan: jsonArray[i]['Kecamatan'],
                        telepon: jsonArray[i]['No Telp'],
                        statusPinjaman: jsonArray[i]['Status Pinjaman(Approved/ Rejected)'],
                        approvalAmount: approvalAmount,
                        approvalTenor: jsonArray[i]['Approval Tenor'],
                        angsuran: jsonArray[i]['Angsuran'],
                        bunga: jsonArray[i]['Bunga'],
                        statusEmailSent: 0
                      })
                    }
                    var formCollection = Bookshelf.Collection.extend({ model: Forms })
                    var inputanfix = formCollection.forge(inputan)
                    Promise.all(inputanfix.invoke('save')).then(function (results) {
                      return reply({statusCode: 200, data: results})
                    }).catch(function (err) {
                      return reply(Boom.badData('Failed to insert to table with error: ' + err))
                    })
                  } else {
                    return reply(Boom.badData('Failed to convert csv to JSON with error: ' + err))
                  }
                })
                fs.createReadStream('./uploads/' + files.files[0].originalFilename).pipe(CsvConverter)
              }
            })
          })
        }
      })
    },
    getList: function getList (request, reply) {
      Forms.forge().fetchAll().then(function (result) {
        result = result.toJSON()
        return reply.view('list', result)
      })
    },
    generateSuratHutang: function generateSuratHutang (request, reply) {
      var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September',
                    'Oktober', 'November', 'Desember']
      var tanggal = new Date().getDate()
      var _bulan = new Date().getMonth()
      var tahun = new Date().getFullYear()
      bulan = bulan[_bulan]

      var terbit = tanggal + ' ' + bulan + ' ' + tahun
      Forms.forge().fetchAll().then(function (result) {
        result = result.toJSON()
        result.forEach(function (data) {
          if (data.decision === 'Rejected') {
            console.log('rejected')
          }
          data.terbit = terbit
          result = data
        })
        return reply.view('surat-hutang', result)
      })
    }
  }
})()

var uploadController = new UploadController()
module.exports = uploadController
