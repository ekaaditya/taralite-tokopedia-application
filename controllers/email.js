'use strict'

var Boom = require('boom')
var helper = require('sendgrid').mail
var sg = require('sendgrid')('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')
var fs = require('fs')

// Function for sending email with attachment
function emailSent (attachData, attachment, mail, personalization, content) {
  if (attachData || attachData !== '') {
    attachment.setContent(attachData)
    attachment.setType('application/pdf')
    attachment.setFilename('Kontrak.pdf')
    attachment.setDisposition('attachment')
    mail.addAttachment(attachment)
  }
  mail.addPersonalization(personalization)
  mail.addContent(content)
  var request = sg.emptyRequest({
    method: 'POST',
    path: '/v3/mail/send',
    body: mail.toJSON()
  })
  var exec = sg.API(request).then(function (response) {
    if (response) {
      return 'hello'
    }
  }).catch(function (err) {
    return reply(Boom.badData('Gagal kirim email. ' + err))
  })
  // var sgs = sg.API(request, function(error, response) {
  //   if (error) {
  //     console.log('Error response received');
  //   }
  //   console.log(response.statusCode)
  //   console.log(response.body)
  //   console.log(response.headers)
  //   return 'senpai'
  // })
}

function EmailController () {}
EmailController.prototype = (function () {
  return {
    sendEmail: function sendEmail (request, reply) {
      if (request.payload.name || request.payload.email) {
        var mail = new helper.Mail()
        var personalization = new helper.Personalization()
        var emailFrom = new helper.Email('cs@taralite.com', 'Taralite CS')
        mail.setFrom(emailFrom)
        mail.setSubject('Application Form Rejected from Taralite')
        mail.setReplyTo(emailFrom)
        var emailTo = new helper.Email(request.payload.email, request.payload.name)
        var content = new helper.Content('text/html', '<html><body>Dear ' + request.payload.name + '</body></html>')
        personalization.addTo(emailTo)

        // Example put attachment to email from local directory server (generate_pdf)
        var attachData = ''
        var attachment = new helper.Attachment()
        fs.readFile('generate_pdf/sample.pdf', function (err, data) {
          if (err) {
            console.log('gagal')
            return reply(Boom.badData('Gagal mendapatkan file attachment. ' + err))
          } else {
            attachData = new Buffer(data).toString('base64')
            var a = emailSent(attachData, attachment, mail, personalization, content)
            console.log('data', a)
            return reply({isi: a})
          }
        })
      } else {
        return reply(Boom.badData('Salah parameter.'))
      }
    }
  }
})()

var emailController = new EmailController()
module.exports = emailController
