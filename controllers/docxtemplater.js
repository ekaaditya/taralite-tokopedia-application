'use strict'

// var multiparty = require('multiparty')
// var fs = require('fs')
// var Path = require('path')
// var Converter = require('csvtojson').Converter
// var Promise = require('bluebird')
// var Bookshelf = require('../config/bookshelf')
// var Forms = require('../models/forms')
// var Boom = require('boom')
var JSZip = require('jszip')
var Docxtemplater = require('docxtemplater')

var fs = require('fs')
var path = require('path')
var livedocx = require('node-livedocx')
var options

function GenerateDocxTemplater () {}
GenerateDocxTemplater.prototype = (function () {
  return {
    generate: function generate (request, reply) {
      var content = fs.readFileSync(path.resolve(__dirname, 'file.docx'), 'binary')
      var zip = new JSZip(content)
      var doc = new Docxtemplater()
      doc.loadZip(zip)
      // var fn = request.payload.first_name
      // var ln = request.payload.last_name
      // var phone = request.payload.phone
      // var description = request.payload.description
      // doc.setData({
      //   first_name: fn,
      //   last_name: ln,
      //   phone: phone,
      //   description: description
      // })
      doc.setData({
        nama_peminjam: 'Jonathan'
      })
      try {
        doc.render()
      } catch (error) {
        var e = {
          message: error.message,
          name: error.name,
          stack: error.stack,
          properties: error.properties
        }
        console.log(JSON.stringify({error: e}))
        throw error
      }
      var buf = doc.getZip()
                   .generate({type: 'nodebuffer'})
      fs.writeFileSync(path.resolve(__dirname, 'output.odt'), buf)
      var config = {
        username: 'jpdg',
        password: 'admin123'
      }
      if ((config.username === '') || (config.password === '')) {
        return console.log('A username and password are required.')
      }
      options = {
        template: fs.readFileSync(path.resolve(__dirname, 'output.docx').toString('base64'))
      }
      livedocx({
        username: config.username,
        password: config.password,
        templateFormat: 'DOCX',
        template: options.template,
        resultFormat: 'PDF'
      }, function (err, resultBuffer) {
        if (err) {
          return console.log('Something wrong', err)
        }
        fs.writeFile('result.pdf', resultBuffer)
        console.log('PDF written to disk')
      })
    }
  }
})()

var docxtemplater = new GenerateDocxTemplater()
module.exports = docxtemplater
