'use strict'

var Template = require('../models/template')

function TemplateController () {}
TemplateController.prototype = (function () {
  return {
    getAllTemplate: function getAllTemplate (request, reply) {
      Template.forge().fetchAll().then(function (result) {
        console.log(result.toJSON())
        reply({data: result})
      })
    },
    getTemplateById: function getTemplateById (request, reply) {
      console.log(request.params)
      Template.forge({id: request.params.id}).fetch().then(function (result) {
        console.log(result.toJSON())
        reply({data: result})
      })
    },
    insert: function insert (request, reply) {
      Template.forge(request.payload).save().then(function (result) {
        console.log(result)
        reply({message: 'successfully inserted a new template'})
      })
    }
  }
})()

var templateController = new TemplateController()
module.exports = templateController
