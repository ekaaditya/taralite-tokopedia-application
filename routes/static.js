'use strict'

module.exports = (function () {
  return [
    {
      method: 'GET',
      path: '/js/{filename}',
      config: {
        handler: function (request, reply) {
          console.log(request.params)
          var path = '/Users/jp/Workspaces/taralite-tokopedia-application/public/assets/js/' + request.params.filename
          console.log(path)
          reply.file(path)
        }
      }
    }
  ]
}())
