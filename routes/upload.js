'use strict'

var uploadController = require('../controllers/upload')

module.exports = (function () {
  return [
    {
      method: 'GET',
      path: '/upload',
      config: {
        handler: function (request, reply) {
          reply.view('upload', {
            title: 'HW',
            message: 'Hello Worlds'
          })
        }
      }
    },
    {
      method: 'POST',
      path: '/upload',
      config: {
        payload: {
          output: 'stream',
          parse: false,
          maxBytes:209715200
        },
        handler: uploadController.upload
      }
    },
    {
      method: 'GET',
      path: '/get-list',
      config: {
        handler: uploadController.getList
      }
    },
    {
      method: 'GET',
      path: '/kontrak',
      config: {
        handler: uploadController.generateSuratHutang
      }
    }
  ]
}())
