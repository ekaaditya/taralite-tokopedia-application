'use strict'

var emailController = require('../controllers/email')

module.exports = (function () {
  return [
    {
      method: 'POST',
      path: '/send',
      config: {
        handler: emailController.sendEmail
      }
    }
  ]
}())
