'use strict'

var pdfController = require('../controllers/pdf')
var docxtemplater = require('../controllers/docxtemplater')

module.exports = (function () {
  return [
    {
      method: 'POST',
      path: '/generate',
      config: {
        handler: pdfController.generate
      }
    },
    {
      method: 'POST',
      path: '/try-generate',
      config: {
        handler: docxtemplater.generate
      }
    }
  ]
}())
