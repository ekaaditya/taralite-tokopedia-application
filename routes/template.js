'use strict'

var templateController = require('../controllers/template')

module.exports = (function () {
  return [
    {
      method: 'GET',
      path: '/template',
      config: {
        handler: templateController.getAllTemplate
      }
    },
    {
      method: 'GET',
      path: '/template/{id}',
      config: {
        handler: templateController.getTemplateById
      }
    },
    {
      method: 'POST',
      path: '/template',
      config: {
        handler: templateController.insert
      }
    }
  ]
}())
