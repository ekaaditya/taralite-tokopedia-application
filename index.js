'use strict'

require('dotenv').config()
var Hapi = require('hapi')
var constants = require('./config/constants.js')
var routes = require('./routes')
var Vision = require('vision')
var Inert = require('inert')
var Path = require('path')

var host = constants.application['host']
var port = constants.application['port']
const server = new Hapi.Server()

server.connection({
  port: port,
  host: host,
  routes: {
    cors: {origin: ['*']},
    files: {
      relativeTo: Path.join(__dirname, 'public')
    }
  }
})

server.register([Vision, Inert], (err) => {
  if (err) console.log(err)
  server.views({
    engines: {
      html: require('handlebars')
    },
    path: Path.join(__dirname, '/views'),
    layoutPath: Path.join(__dirname, '/views/layout'),
    layout: 'default'
  })
})

// Add all the routes within the routes folder
for (var route in routes) {
  server.route(routes[route])
}

module.exports = server

if (process.env.NODE_ENV !== 'test') {
  server.start()

  console.log('Server running in port #' + port)
}
