var database = {
  'production': {
    'client': 'postgres',
    connection: {
      'host': process.env.DB_PRD_HOST,
      'user': process.env.DB_PRD_USER,
      'password': process.env.DB_PRD_PASS,
      'database': 'tokopedia-app'
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: './database/migrations'
    },
    seeds: {
      directory: '../database/seeds'
    }
  },
  'development': {
    'client': 'postgres',
    connection: {
      'host': 'localhost',
      'user': process.env.DB_DEV_USER,
      'password': process.env.DB_DEV_PASS,
      'database': 'tokopedia-app'
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: './database/migrations'
    },
    seeds: {
      directory: './database/seeds'
    }
  },
  'test': {
    'client': 'postgres',
    connection: {
      'host': 'localhost',
      'user': process.env.DB_TEST_USER,
      'password': process.env.DB_TEST_PASS,
      'database': 'tokopedia-app-test'
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: './database/migrations'
    },
    seeds: {
      directory: './database/seeds'
    }
  }
}
module.exports = database
