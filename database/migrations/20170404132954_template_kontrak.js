
exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.createTable('template', function (table) {
      table.increments('id')
      table.string('judul')
      table.string('konten')
      table.timestamp('createdAt').notNullable().defaultTo(knex.raw('now()'))
      table.timestamp('updatedAt').notNullable().defaultTo(knex.raw('now()'))
    })
  ])
}

exports.down = function (knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('template')
  ])
}
