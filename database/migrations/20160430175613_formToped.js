exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.createTable('formToped', function (table) {
      table.increments('id')
      table.string('nama')
      table.string('email')
      table.string('tipePinjaman')
      table.string('tujuanPinjaman')
      table.text('alamat')
      table.string('kecamatan')
      table.string('telepon')
      table.string('statusPinjaman')
      table.integer('approvalAmount')
      table.string('approvalTenor')
      table.string('angsuran')
      table.string('bunga')
      table.integer('statusEmailSent')
      table.string('pdfLink1')
      table.string('pdfLink2')
      table.string('pdfLink3')
      table.timestamp('createdAt').notNullable().defaultTo(knex.raw('now()'))
      table.timestamp('updatedAt').notNullable().defaultTo(knex.raw('now()'))
    })
  ])
}

exports.down = function (knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('formToped')
  ])
}
