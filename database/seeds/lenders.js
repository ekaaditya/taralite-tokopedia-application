exports.seed = function (knex, Promise) {
  return Promise.join(
    // Deletes ALL existing entries
    knex('lenders').del(),

    // Inserts seed entries
    knex('lenders').insert({
      id: 1,
      name: 'Tony Stark',
      email: 'tonystark@gmail.com',
      password: 'password',
      mobilePhone: '0812'
    }),
    knex('lenders').insert({
      id: 2,
      name: 'Xavier',
      email: 'xavier@gmail.com',
      password: 'password',
      mobilePhone: '0813'
    }),
    knex('lenders').insert({
      id: 3,
      name: 'Barry Allen',
      email: 'barry@gmail.com',
      password: 'password',
      mobilePhone: '0814'
    }),
    knex('lenders').insert({
      id: 4,
      name: 'Clark',
      email: 'clark@gmail.com',
      password: 'password',
      mobilePhone: '0815'
    })
  )
}
