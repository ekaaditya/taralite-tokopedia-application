# Taralite Service Boilerplate

version 1.0.0

## Installation

```bash
1. clone this repository
2. npm Install
3. create migration (or you can do manually on your database)
4. run migration (skip this if you not do step 3)
```

## Create migrations
```bash
knex migrate:make migrate_name

nb: you need install knex as global: npm i knex -g
```

## Run migrations
```bash
knex migrate:latest
```

## Command
```bash
1. npm start : to run Hapi server, default run on localhost:3000
2. npm run lint : lint all .js file
3. npm run test : run unit test using mocha

4. knex migrate:make migrate_name : create migration file
5. knex migrate:latest : migrate database

6. knex seed:make seed_name : create side file to dummy data
7. knex seed:run : add dummy data to database
```
